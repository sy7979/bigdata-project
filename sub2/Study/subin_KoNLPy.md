# KoNLPy

## KoNLPy로 품사 태깅하기

> KoNLPy에는 품사 태깅을 하기 위한 옵션이 여럿 있는데, 이들은 모두 문구(phrase)를 입력받아 태깅된 형태소를 출력하는 동일한 입출력 구조를 가집니다.

### 품사 태깅 클래스 간 비교

#### 로딩 시간

> 사전 로딩을 포함하여 클래스를 로딩하는 시간
>
> 짧은 순서부터 나타냄
>
> Mecab < Hannanum < Okt < Komoran < Kkma

#### 실행 시간

> 10만 문자의 문서를 대상으로 각 클래스의 `pos` 메소드를 실행하는데 소요되는 시간
>
> Mecab < Okt < Hannanum < Komoran < Kkma
>
> 문자의 개수가 늘어남에 따라 실행 시간이 기하급수적으로 늘어남

### 성능 분석

#### 아버지가방에들어가신다

> 띄어쓰기 알고리즘의 성능을 확인해볼 수 있음
>
> "아버지가 + 방에 + 들어가신다"가 이상적임

| Hannanum               | Kkma         | Komoran                      | Mecab        | Twitter         |
| ---------------------- | ------------ | ---------------------------- | ------------ | --------------- |
| 아버지가방에들어가 / N | 아버지 / NNG | 아버지가방에들어가신다 / NNP | 아버지 / NNG | 아버지 / Noun   |
| 이 / J                 | 가방 / NNG   |                              | 가 / JKS     | 가방 / Noun     |
| 시ㄴ다 / E             | 에 / JKM     |                              | 방 / NNG     | 에 / Josa       |
|                        | 들어가 / VV  |                              | 에 / JKB     | 들어가신 / Verb |
|                        | 시 / EPH     |                              | 들어가 / VV  | 다 / Eomi       |
|                        | ㄴ다 / EFN   |                              | 신다 / EP+EC |                 |

```markdown
띄어쓰기 알고리즘은 Mecab이 우세하다고 보여진다.
그 다음으로는 Kkma라고 볼 수 있다.
```

<br/>

#### 나는 밥을 먹는다 VS 하늘을 나는 자동차

> 단어의 의미와 주변부를 살펴 어떤 의미를 가지는지에 대한 알고리즘 성능을 확인해볼 수 있음
>
>  첫번째 문장에서 "나는"은 `나/N + 는/J`, 두번째 문장에서는 `나(-ㄹ다)/V + 는/E` 이 되는 것이 바람직함

| Hannanum | Kkma     | Komoran   | Mecab     | Twitter     |
| -------- | -------- | --------- | --------- | ----------- |
| 나 / N   | 나 / NP  | 나 / NP   | 나 / NP   | 나 / Noun   |
| 는 / J   | 는 / JX  | 는 / JX   | 는 / JX   | 는 / Josa   |
| 밥 / N   | 밥 / NNG | 밥 / NNG  | 밥 / NNG  | 밥 / Noun   |
| 을 / J   | 을 / JKO | 을 / JKO  | 을 / JKO  | 을 / Josa   |
| 먹 / P   | 먹 / VV  | 먹 / VV   | 먹 / VV   | 먹는 / Verb |
| 는다 / E | 는 / EPT | 는다 / EC | 는다 / EC | 다 / Eomi   |
|          | 다 / EFN |           |           |             |

| Hannanum   | Kkma         | Komoran      | Mecab        | Twitter       |
| ---------- | ------------ | ------------ | ------------ | ------------- |
| 하늘 / N   | 하늘 / NNG   | 하늘 / NNG   | 하늘 / NNG   | 하늘 / Noun   |
| 을 / J     | 을 / JKO     | 을 / JKO     | 을 / JKO     | 을 / Josa     |
| 나 / N     | 날 / VV      | 나 / NP      | 나 / NP      | 나 / Noun     |
| 는 / J     | 는 / ETD     | 는 / JX      | 는 / JX      | 는 / Josa     |
| 자동차 / N | 자동차 / NNG | 자동차 / NNG | 자동차 / NNG | 자동차 / Noun |

```markdown
"나는 밥을 먹는다"에서 "나는"은 모두 잘 분리되었다.
하지만 "하늘을 나는 자동차"에서 "나는"은 "날다"와 "는"이 합해진 것이다.
가장 잘 나눠진 것은 Kkma라고 할 수 있다.
```

<br/>

#### 아이폰 기다리다 지쳐 애플공홈에서 언락폰질러버렸다 6+ 128기가실버ㅋ

> 사전에 포함되지 않은 단어를 구분하는 알고리즘의 성능을 확인할 수 있음

| Hannanum             | Kkma        | Komoran          | Mecab                  | Twitter             |
| -------------------- | ----------- | ---------------- | ---------------------- | ------------------- |
| 아이폰 / N           | 아이 / NNG  | 아이폰 / NNP     | 아이폰 / NNP           | 아이폰 / Noun       |
| 기다리 / P           | 폰 / NNG    | 기다리 / VV      | 기다리 / VV            | 기다리 / Verb       |
| 다 / E               | 기다리 / VV | 다 / EC          | 다 / EC                | 다 / Eomi           |
| 지치 / P             | 다 / ECS    | 지치 / VV        | 지쳐 / VV+EC           | 지쳐 / Verb         |
| 어 / E               | 지치 / VV   | 어 / EC          | 애플 / NNP             | 애플 / Noun         |
| 애플공홈 / N         | 어 / ECS    | 애플 / NNP       | 공 / NNG               | 공홈 / Noun         |
| 에서 / J             | 애플 / NNP  | 공 / NNG         | 홈 / NNG               | 에서 / Josa         |
| 언락폰질러버렸다 / N | 공 / NNG    | 홈 / NNG         | 에서 / JKB             | 언락폰 / Noun       |
| 6+ / N               | 홈 / NNG    | 에서 / JKB       | 언락 / NNG             | 질 / Verb           |
| 128기가실벜 / N      | 에서 / JKM  | 언 / NNG         | 폰 / NNG               | 러 / Eomi           |
|                      | 언락 / NNG  | 락 / NNG         | 질러버렸 / VV+EC+VX+EP | 버렸 / Verb         |
|                      | 폰 / NNG    | 폰 / NNG         | 다 / EC                | 다 / Eomi           |
|                      | 질르 / VV   | 지르 / VV        | 6 / SN                 | 6 / Number          |
|                      | 어 / ECS    | 어 / EC          | + / SY                 | + / Punctuation     |
|                      | 버리 / VXV  | 버리 / VX        | 128 / SN               | 128 / Number        |
|                      | 었 / EPT    | 었 / EP          | 기 / NNG               | 기 / Noun           |
|                      | 다 / ECS    | 다 / EC          | 가 / JKS               | 가 / Josa           |
|                      | 6 / NR      | 6 / SN           | 실버 / NNP             | 실버 / Noun         |
|                      | + / SW      | + / SW           | ㅋ / UNKNOWN           | ㅋ / KoreanParticle |
|                      | 128 / NR    | 128기가실벜 / NA |                        |                     |
|                      | 기가 / NNG  |                  |                        |                     |
|                      | 실버 / NNG  |                  |                        |                     |
|                      | ㅋ / UN     |                  |                        |                     |

```markdown
가장 잘 나눠진 것이라고 판단되는 것은 Twitter이다.
```

<br/>

## Hannanum Class

```python
from konlpy.tag import Hannanum


hannanum = Hannanum()

print(hannanum.analyze(u'롯데마트의 흑마늘 양념 치킨이 논란이 되고 있다.'))
[[[('롯데마트', 'ncn'), ('의', 'jcm')], [('롯데마트의', 'ncn')], [('롯데마트', 'nqq'), ('의', 'jcm')], [('롯데마트의', 'nqq')]], [[('흑마늘', 'ncn')], [('흑마늘', 'nqq')]], [[('양념', 'ncn')]], [[('치킨', 'ncn'), ('이', 'jcc')], [('치킨', 'ncn'), ('이', 'jcs')], [('치킨', 'ncn'), ('이', 'ncn')]], [[('논란', 'ncpa'), ('이', 'jcc')], [('논란', 'ncpa'), ('이', 'jcs')], [('논란', 'ncpa'), ('이', 'ncn')]], [[('되', 'nbu'), ('고', 'jcj')], [('되', 'nbu'), ('이', 'jp'), ('고', 'ecc')], [('되', 'nbu'), ('이', 'jp'), ('고', 'ecs')], [('되', 'nbu'), ('이', 'jp'), ('고', 'ecx')], [('되', 'paa'), ('고', 'ecc')], [('되', 'paa'), ('고', 'ecs')], [('되', 'paa'), ('고', 'ecx')], [('되', 'pvg'), ('고', 'ecc')], [('되', 'pvg'), ('고', 'ecs')], [('되', 'pvg'), ('고', 'ecx')], [('되', 'px'), ('고', 'ecc')], [('되', 'px'), ('고', 'ecs')], [('되', 'px'), ('고', 'ecx')]], [[('있', 'paa'), ('다', 'ef')], [('있', 'px'), ('다', 'ef')]], [[('.', 'sf')], [('.', 'sy')]]]

print(hannanum.morphs(u'롯데마트의 흑마늘 양념 치킨이 논란이 되고 있다.'))
['롯데마트', '의', '흑마늘', '양념', '치킨', '이', '논란', '이', '되', '고', '있', '다', '.']

print(hannanum.nouns(u'다람쥐 헌 쳇바퀴에 타고파'))
['다람쥐', '쳇바퀴', '타고파']

print(hannanum.pos(u'웃으면 더 행복합니다!'))
[('웃', 'P'), ('으면', 'E'), ('더', 'M'), ('행복', 'N'), ('하', 'X'), ('ㅂ니다', 'E'), ('!', 'S')]
```

<br/>

## Kkma Class

```python
from konlpy.tag import Kkma


kkma = Kkma()

print(kkma.morphs(u'공부를 하면할수록 모르는게 많다는 것을 알게 됩니다.'))
['공부', '를', '하', '면', '하', 'ㄹ수록', '모르', '는', '것', '이', '많', '다는', '것', '을', '알', '게', '되', 'ㅂ니다', '.']

print(kkma.nouns(u'대학에서 DB, 통계학, 이산수학 등을 배웠지만...'))
['대학', '통계학', '이산', '이산수학', '수학', '등']

print(kkma.pos(u'다 까먹어버렸네요?ㅋㅋ'))
[('다', 'MAG'), ('까먹', 'VV'), ('어', 'ECD'), ('버리', 'VXV'), ('었', 'EPT'), ('네요', 'EFN'), ('?', 'SF'), ('ㅋㅋ', 'EMO')]

print(kkma.sentences(u'그래도 계속 공부합니다. 재밌으니까!'))
['그래도 계속 공부합니다.', '재밌으니까!']
```

> **Kkma는 단어 사이에 띄어쓰기 없는 긴 문장에 약하다.**

<br/>

## Komoran Class

```python
cat /tmp/dic.txt  # Place a file in a location of your choice
코모란     NNP
오픈소스    NNG
바람과 함께 사라지다     NNP

from konlpy.tag import Komoran


komoran = Komoran(userdic='/tmp/dic.txt')

print(komoran.morphs(u'우왕 코모란도 오픈소스가 되었어요'))
['우왕', '코모란', '도', '오픈소스', '가', '되', '었', '어요']

print(komoran.nouns(u'오픈소스에 관심 많은 멋진 개발자님들!'))
['오픈소스', '관심', '개발자']

print(komoran.pos(u'혹시 바람과 함께 사라지다 봤어?'))
[('혹시', 'MAG'), ('바람과 함께 사라지다', 'NNP'), ('보', 'VV'), ('았', 'EP'), ('어', 'EF'), ('?', 'SF')]
```

<br/>

## Mecab Class

```python
# MeCab installation needed

from konlpy.tag import Mecab


mecab = Mecab()

print(mecab.morphs(u'영등포구청역에 있는 맛집 좀 알려주세요.'))
['영등포구', '청역', '에', '있', '는', '맛집', '좀', '알려', '주', '세요', '.']

print(mecab.nouns(u'우리나라에는 무릎 치료를 잘하는 정형외과가 없는가!'))
['우리', '나라', '무릎', '치료', '정형외과']

print(mecab.pos(u'자연주의 쇼핑몰은 어떤 곳인가?'))
[('자연', 'NNG'), ('주', 'NNG'), ('의', 'JKG'), ('쇼핑몰', 'NNG'), ('은', 'JX'), ('어떤', 'MM'), ('곳', 'NNG'), ('인가', 'VCP+EF'), ('?', 'SF')]
```

> 아쉽게도 Windows에서는 지원되지 않는다.

<br/>

## Okt class

```python
from konlpy.tag import Okt


okt = Okt()

print(okt.morphs(u'단독입찰보다 복수입찰의 경우'))
['단독', '입찰', '보다', '복수', '입찰', '의', '경우']

print(okt.nouns(u'유일하게 항공기 체계 종합개발 경험을 갖고 있는 KAI는'))
['항공기', '체계', '종합', '개발', '경험']

print(okt.phrases(u'날카로운 분석과 신뢰감 있는 진행으로'))
['날카로운 분석', '날카로운 분석과 신뢰감', '날카로운 분석과 신뢰감 있는 진행', '분석', '신뢰', '진행']

print(okt.pos(u'이것도 되나욬ㅋㅋ'))
[('이', 'Determiner'), ('것', 'Noun'), ('도', 'Josa'), ('되나욬', 'Noun'), ('ㅋㅋ', 'KoreanParticle')]

print(okt.pos(u'이것도 되나욬ㅋㅋ', norm=True))
[('이', 'Determiner'), ('것', 'Noun'), ('도', 'Josa'), ('되나요', 'Verb'), ('ㅋㅋ', 'KoreanParticle')]

print(okt.pos(u'이것도 되나욬ㅋㅋ', norm=True, stem=True))
[('이', 'Determiner'), ('것', 'Noun'), ('도', 'Josa'), ('되다', 'Verb'), ('ㅋㅋ', 'KoreanParticle')]
```

> Twitter가 Okt로 바뀌었다.

<br/>

## 사용법

```python
from konlpy.utils import pprint


kkma = Kkma()

pprint(kkma.sentences(u'네, 안녕하세요. 반갑습니다.'))
[네, 안녕하세요..,
 반갑습니다.]

pprint(kkma.nouns(u'질문이나 건의사항은 깃헙 이슈 트래커에 남겨주세요.'))
[질문,
 건의,
 건의사항,
 사항,
 깃헙,
 이슈,
 트래커]

pprint(kkma.pos(u'오류보고는 실행환경, 에러메세지와함께 설명을 최대한상세히!^^'))
[(오류, NNG),
 (보고, NNG),
 (는, JX),
 (실행, NNG),
 (환경, NNG),
 (,, SP),
 (에러, NNG),
 (메세지, NNG),
 (와, JKM),
 (함께, MAG),
 (설명, NNG),
 (을, JKO),
 (최대한, NNG),
 (상세히, MAG),
 (!, SF),
 (^^, EMO)]
```

<br/>

###### 출처

> http://konlpy.org/ko/latest/
>
> http://konlpy.org/ko/latest/morph/#pos-tagging-with-konlpy
>
> http://konlpy.org/ko/latest/morph/#comparison-between-pos-tagging-classes