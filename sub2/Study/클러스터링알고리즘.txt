클러스터링 알고리즘이란? 
분류되지 않은 개체들이 주어졌을 때, 개체들을 개체간의 거리를 바탕으로
몇 개의 군집으로 나누는 비지도 학습의 일종입니다

1. 클러스터링 알고리즘의 목표는 비슷한 특성의 개체를 같은 군집으로!!
   and 다른(상이한) 특성은 다른 군집으로 묶는 것입니다

1-1 1번의 목표를 끌어내기 위해서는 개체간의 거리(유사도)를 측정
      ****매우중요**** 계산방법에 따라 결과가 달라진다

1-2 SUB3 에서는 클러스터링 알고리즘 중에서도 대용량 자료의 군집의 
     효과적인 **** K-means 클러스터링 알고리즘**** 학습한다

tip : 클러스터링이란?
어떤 데이터들이 주어졌을 때, 그 데이터들을 클러스터로 그루핑 시켜주는것

K-평균 군집화(K-means Clustering) 
각 군집은 하나의 중심(centroid)을 가집니다.
사용자가 사전에 군집수를 정해야 알고리즘을 실행 할 수있다
 K-means Clustering  
( K: 클러스터의 갯수를 뜻합니다 )
( means : 한 클러스터 안의 데이터 중심 centroid) 뜻함)
정리: K개의 centroid를 기반으로 K개의 클러스터를 만들어주는것을 의미

프로세스
1. 얼마나 많은 클러스터가 필요한지 결정( K 결정)
2. 초기 centroid 선택 
2-1) 랜덤하게 설정
2-2) 수동으로 설정
2-3) Kmean++방법
3. 모든 데이터를 순회하며 각 데이터마다 가장 가까운 centroid가 속해있는
   클러스터로 assign
4. centroid를 클러스터의 중심으로 이동
5. 클러스터에 assign 되는 데이터가 없을 때까지 스텝3,4 반복

실습
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
%matplotlib inline

X= -2 * np.random.rand(100,2)
X1 = 1 + 2 * np.random.rand(50,2)
X[50:100, :] = X1
plt.scatter(X[ : , 0], X[ :, 1], s = 50, c = 'b')
plt.show()

실행하면 두개의 클러스터(집합)으로 구분할 수있다는걸 볼수있다

後
from sklearn.cluster import KMeans
Kmean = KMeans(n_clusters=2)
Kmean.fit(X)

클러스터를 2개로 할 것이기 때문에 n_clusters파라미터를 2로 줬습니다
fit을 해주면 아래와 같은결과가 나옵니다.

KMeans(algorithm='auto', copy_x=True, init='k-means++', max_iter=300,
         n_clusters=2, n_init=10, n_jobs=None, precompute_distances='auto',
         random_state=None, tol=0.0001, verbose=0)
주석//
init 파라미터의 default 값은 k-mean++입니다. 
k-mean++는 초기 Centroid를 정하는 방법 중 하나입니다. 
먼저 맨 처음 데이터를 centroid1으로 둡니다. 
cenctoid1으로부터 가장 멀리 떨어져 있는 데이터를 centroid2로 둡니다. centroid1과 centroid2로부터 가장 멀리 떨어져 있는 데이터를 centroid3로 둡니다. 이렇게 K번 반복하여 초기 centroid를 정해주는 것입니다. 
임의로 정하거나 랜덤하게 정하는 것보다 iteration 횟수를 줄여주어 좀 더 효율적입니다.
max_iter=300으로 되어있습니다. 
위에서 설명한 프로세스에서 스텝 3~4를 클러스터에 assign되는 데이터가 없을 때까지 
반복한다고 했습니다. 하지만 max_iter를 정해주면 max_iter까지 하거나 assign 되는 데이터가 없을 때까지 반복합니다. 
웬만하면 스텝 3~4의 iteration이 300번 이전에 끝나기 때문에 default가 300입니다.

Kmean.cluster_centers_

>>> array([[ 2.02664296,  1.88206121],
          [-1.01085055, -1.03792754]])

주석:// 두 Centroid 의 위치입니다 두 centroid 와 함께 plot을 그려보겠습니다.

plt.scatter(X[ : , 0], X[ : , 1], s =50, c='b')
plt.scatter(-0.94665068, -0.97138368, s=200, c='g', marker='s')
plt.scatter(2.01559419, 2.02597093, s=200, c='r', marker='s')
plt.show()

K-means 클러스터링 알고리즘 완료

단!!! 이 알고리즘의 단점이 존재합니다.
문제점의 이름은: local minimum 이라고 한다

왼쪽 2행3열로 묶어져있는 데이터집단과 
오른쪽 2행3열로 묶어져있는 데이터 집단을 나눌 때
초기에 설정된 centroid가 잘못설정하여
왼쪽과 오른쪽 집단으로 나누는게 아닌
1행과 2행으로 나누어버리는 경우가 생길 수가 있다!!

이러한 점을 해결하기 위해서centroid 위치를 잘 지정해야줘야한다

 		