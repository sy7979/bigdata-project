뭐야 왜 안올라가



# TF-IDF(Term Frequency - Inverse Document Frequency) 란?

- TF(단어 빈도, term frequency)는 특정한 단어가 `문서 내에 얼마나 자주 등장하는지를 나타내는 값`. 이 값이 `높을수록 문서에서 중요`하다고 생각할 수 있다.
- 하지만 하나의 문서에서 많이 나오지 않고 `다른 문서에서 자주 등장하면 단어의 중요도는 낮아진다`.
- DF(문서 빈도, document frequency)라고 하며, 이 값의 역수를 IDF(역문서 빈도, inverse document frequency)라고 한다.
- TF-IDF는 TF와 IDF를 곱한 값으로 점수가 높은 단어일수록 `다른 문서에는 많지 않고 해당 문서에서 자주 등장하는 단어`를 의미한다.

# 예제

## Twitter 자연어 처리기

- 자연어 처리를 하기 위해 필요하다.

- https://github.com/open-korean-text/open-korean-text

- Maven

  ```
  <dependency>
    <groupId>org.openkoreantext</groupId>
    <artifactId>open-korean-text</artifactId>
    <version>2.1.0</version>
  </dependency>
  ```

## 문서 정보

- 3가지의 예제 문서가 있다고 가정하자.[![No Image](https://nesoy.github.io/assets/posts/20171107/1.png)](https://nesoy.github.io/assets/posts/20171107/1.png)

```
private static String[] dataList = {
            "I love dogs", // I, love, dogs
            "I hate dogs and knitting", // I, hate, dogs, and, knitting
            "Knitting is my hobby and my passion", // Knitting, is, my, hobby, and, my, passion
    };
```

## Tf-IDF 모델 정의

```
class Tf_idf {
    private int tf = 0;
    private double idf = 0;

    public void addTf() {
        this.tf = tf + 1;
    }

    public void setIdf(double idf) {
        this.idf = idf;
    }

    public double getTf_idf() {
        return tf * idf;
    }

    @Override
    public String toString() {
        return String.format("tf = %d | idf = %.2f | tf-idf = %.2f", tf, idf, getTf_idf());
    }
}
```

## Tf값 계산하기

- tf값을 계산하는 방법에는 다양한 방법이 있다.

  - 문서의 길이에 따라 단어의 빈도값을 조절할 수도 있다.
  - boolean 빈도로 한번만 등장해도 1로 값을 정하는 경우도 있다.
  - log를 사용하여 값을 조절할 수도 있다.

- 아래 방법은 기본적으로 나온 횟수를 count한다.

  ```
  // Tf 계산
  for (String data : dataList) {
    // Normalize - 자연어 처리
    CharSequence normalized = OpenKoreanTextProcessorJava.normalize(data);
  
    // Tokenize - 자연어 처리
    Seq<KoreanTokenizer.KoreanToken> tokens = OpenKoreanTextProcessorJava.tokenize(normalized);
  
    // documentMap 생성
    HashMap<String, Tf_idf> documentMap = new HashMap<String, Tf_idf>();
  
    // documentMap에 Tf값 입력
    for (String token : OpenKoreanTextProcessorJava.tokensToJavaStringList(tokens)) {
        if (!documentMap.containsKey(token)) {  // document에 token이 없을 경우
            Tf_idf tokenValue = new Tf_idf();
            tokenValue.addTf();
  
            documentMap.put(token, tokenValue);
        } else {                                // document에 token이 있는 경우
            Tf_idf tokenValue = documentMap.get(token);
            tokenValue.addTf();
        }
    }
  
    // documentList에 documentMap 추가
    documentList.add(documentMap);
  }
  ```

- 결과값

[![No Image](https://nesoy.github.io/assets/posts/20171107/tf.png)](https://nesoy.github.io/assets/posts/20171107/tf.png)

## Idf값 계산

- `log(전체문서의 수 / token이 포함된 문서의 수)`

```
// idf 계산
for (HashMap documentMap : documentList) {
    Iterator<String> tokenList = documentMap.keySet().iterator(); // document token 가져오기

    while (tokenList.hasNext()) {
        String token = tokenList.next();
        Tf_idf tokenValue = (Tf_idf) documentMap.get(token);

        int hit_document = 0;

        for (int index = 0; index < documentList.size(); index++) {
            if (documentList.get(index).containsKey(token)) { // document에 token이 포함한 경우
                hit_document++;
            }
        }
        tokenValue.setIdf(Math.log10((double) documentList.size() / hit_document)); //  log(전체 문서 / hit 문서)
    }
}
```

- 결과값
  - `tf-idf`값이 높을수록 다른 문서에 잘 언급되지 않은 단어(my, love, hate, hobby, is, passion)인 것을 알 수 있다.
  - `tf-idf`값이 낮을수록 다른 문서에 잘 언급하는 단어(I, dogs, and, knitting)인 것을 알 수 있다.

[![No Image](https://nesoy.github.io/assets/posts/20171107/idf.png)](https://nesoy.github.io/assets/posts/20171107/idf.png)

[![No Image](https://nesoy.github.io/assets/posts/20171107/result.png)](https://nesoy.github.io/assets/posts/20171107/result.png)





# KONLPY

자연어 처리에서 각 언어마다 모두 특징이 다르기 때문에 동일한 방법을 사용하기는 어려울 것이다. 한글에도 NLTK나 Spacy 같은 도구를 사용할 수 있으면 좋겠지만 언어 특성상 영어를 위한 도구를 사용하기에는 적합하지 않다. 하지만 많은 사람들의 노력으로 개발된 한글 자연어 처리를 돕는 훌륭한 도구를 사용할 수있다. 그중 한글 자연어 처리에 많이 사용하는 파이썬 라이브버리 KoNLPy에 대해 알아보겠다.

![knn](https://devtimes.com/assets/img/post/konlpy/konlpy.png)

KoNLPy는 한글 자연어 처리를 쉽고 간결하게 처리할 수 있도록 만들어진 오픈소스 라이브러리다. 또한 국내에 이미 만들어져 사용되고 있는 여러 형태소 분석기를 사용할 수 있게 허용한다. 일반적인 어절 단위에 대한 토크나이징은 NLTK로 충분히 해결할 수 있으므로 형태소 단위에 대한 토크나이징에 대해 알아보도록 하겠다.

# 설치[Permalink](https://devtimes.com/bigdata/2019/04/18/konlpy/#설치)

리눅스 또는 macOS에서는 다음과 같이 pip를 이용해 간단하게 설치할 수 있다.

```
pip install konlpy #python2.x
pip3 install konlpy #python2.x
```

# 형태소 단위 토크나이징[Permalink](https://devtimes.com/bigdata/2019/04/18/konlpy/#형태소-단위-토크나이징)

한글 텍스트의 경우 형태소 단위 토크나이징이 필요할 떄가 있는데 KoNLPy에서는 여러 형태소 분석기를 제공하며, 각 형태소 분석기별로 분석한 결과는 다를 수 있다. 각 형태소 분석기는 클래스 형태로 되어 있고 이를 객체로 생성한 후 매서드를 호출해서 토크나이징할 수 있다.

# 형태소 분석 및 품사 태깅[Permalink](https://devtimes.com/bigdata/2019/04/18/konlpy/#형태소-분석-및-품사-태깅)

형태소란 의미를 가지는 가장 작은 단위로서 더 쪼개지면 의미를 상실하는 것들을 말한다. 따라서 형태소 분석이란 의미를 가지는 단위를 기준으로 문장을 살펴보는 것을 의미한다. KoNLPy는 기존에 C, C++, Java 등의 언어를 통해 형태소 분석을 할 수 있는 좋은 라이브러리들을 파이썬 라이브러리로 통합해서 사용할 수 있록 하여 한국어 구문 분석을 쉽게 할 수 있도록 만들어진 라이브러리이다. KoNLPy에는 다양한 형태소 분석기들이 객체 형태로 포함돼 있으며 다음과 같은 각 형태소 분석기 목록이 있다.

- Hannanum
- Kkma
- Komoran
- Mecab
- Okt(Twitter)

모두 동일한 형태소 분석기능을 제공하는데, 각기 성능이 조금씩 다르다고 하니 직접 비교해보고 자신의 데이터를 가장 잘 분석하는 분석기를 사용하는 것이 좋다. (단, Mecab는 윈도우에서 사용할 수 없다.)

여기에서는 Okt 예로 들어 설명 하도록 하겠다. Okt는 원래 이름이 Twitter였으나 0.5.0 버전 이후부터 이름이 Okt로 바뀌었다.

```
import konlpy.tag import Okt

okt = Okt() # 객체 생성
```

Okt 에서 제공되는 함수를 살펴보자.

- okt.morphs() 텍스트를 형태소 단위로 나눈다. 옵션으로 norm과 stem이 있다. norm은 문장을 정규화. stem은 각 단어에서 어간을 추출.(기본값은 둘다 False)
- okt.nouns() 텍스트에서 명사만 뽑아낸다.
- okt.phrases() 텍스트에서 어절을 뽑아낸다.
- okt.pos() 각 품사를 태깅하는 역할을 한다. 품사를 태깅한다는 것은 주어진 텍스트를 형태소 단위로 나누고, 나눠진 각 형태소를 그에 해당하는 품사와 함께 리스트화하는 것을 의미한다. 옵션으로 norm, stem, join이 있는데 join은 나눠진 형태소와 품사를 ‘형태소/품사’ 형태로 같이 붙여서 리스트화한다.

다음 문장을 직접 각 함수에 적용해서 살펴보자.

**모바일 게임은 재밌다 열심히 해서 만랩을 찍어야지~ ㅎㅎㅎ**

```
from konlpy.tag import Okt
okt = Okt()

text = "모바일 게임은 재밌다 열심히 해서 만랩을 찍어야지~ ㅎㅎㅎ"

print(okt.morphs(text))
print(okt.morphs(text, stem=True))
['모바일', '게임', '은', '재밌다', '열심히', '해서', '만', '랩', '을', '찍어야지', '~', 'ㅎㅎㅎ']
['모바일', '게임', '은', '재밌다', '열심히', '하다', '만', '랩', '을', '찍다', '~', 'ㅎㅎㅎ']
```

어간 추출을 한 경우 `찍어야지`의 어간인 `찍다`로 추출된 것을 볼 수 있다.

이제 명사와 어절을 추츨헤 보자

```
print(okt.nouns(text))
print(okt.phrases(text))
['모바일', '게임', '랩']
['모바일', '모바일 게임', '만랩', '게임']
```

nouns 함수를 사용한 경우에는 명사만 추출되었고 phrases 함수의 경우 어절 단위로 나뉘어서 추출 되었다.

품사 태깅을 하는 함수 pos를 사용해보자.

```
print(okt.pos(text))
print(okt.pos(text, join=True))
[('모바일', 'Noun'), ('게임', 'Noun'), ('은', 'Josa'), ('재밌다', 'Adjective'), ('열심히', 'Adverb'), ('해서', 'Verb'), ('만', 'Modifier'), ('랩', 'Noun'), ('을', 'Josa'), ('찍어야지', 'Verb'), ('~', 'Punctuation'), ('ㅎㅎㅎ', 'KoreanParticle')]
['모바일/Noun', '게임/Noun', '은/Josa', '재밌다/Adjective', '열심히/Adverb', '해서/Verb', '만/Modifier', '랩/Noun', '을/Josa', '찍어야지/Verb', '~/Punctuation', 'ㅎㅎㅎ/KoreanParticle']
```

join 옵션을 True로 설정 하면 형태소와 품사가 함께 나오는 것을 볼 수 있다. 경우에 따라 옵션을 설정하면서 사용하면 된다.

# KoNLPy 데이터[Permalink](https://devtimes.com/bigdata/2019/04/18/konlpy/#konlpy-데이터)

KoNLPy 라이브러리는 한글 자연어 처리에 활용할 수 있는 한글 데이터를 포함하고 있어 라이브러리를 통해 데이터를 바로 사용할 수 있다.

- kolaw 한국 법률 말뭉치. ‘constitution.txt’파일

- kobill 대한민국 국회 의안 말뭉치. 각 id값을 가지는 의안으로 구성. 파일은 ‘1809890.txt’ 부터 ‘1809899.txt’까지로 구성.

  라이브러리를 사용해 각 말뭉치를 불러오자.

```
from konlpy.corpus import kolaw
from konlpy.corpus import kobill

kolaw.open('constitution.txt').read()[:30]
'대한민국헌법\n\n유구한 역사와 전통에 빛나는 우리 대한국'
kobill.open('1809890.txt').read()[:30]
'지방공무원법 일부개정법률안\n\n(정의화의원 대표발의 )\n'
```

위 데이터들을 가지고 여러 가지 한글 자연어 처리 문제를 연습하는 데 활용할 수 있다.







## K-means 알고리즘 

KC는 대표적인 [분리형 군집화 알고리즘](https://ratsgo.github.io/machine learning/2017/04/16/clustering/) 가운데 하나입니다. 각 군집은 하나의 **중심(centroid)**을 가집니다. 각 개체는 가장 가까운 중심에 할당되며, 같은 중심에 할당된 개체들이 모여 하나의 군집을 형성합니다. 사용자가 사전에 군집 수(kk)가 정해야 알고리즘을 실행할 수 있습니다. kk가 **하이퍼파라메터(hyperparameter)**라는 이야기입니다. 이를 수식으로 적으면 아래와 같습니다.

X=C1∪C2...∪CK,Ci∩Cj=ϕargminC∑i=1K∑xj∈Ci∥xj−ci∥2X=C1∪C2...∪CK,Ci∩Cj=ϕargminC∑i=1K∑xj∈Ci‖xj−ci‖2

## 학습 과정

KC는 **EM 알고리즘**을 기반으로 작동합니다. EM알고리즘은 크게 **Expectation** 스텝과 **Maximization** 스텝으로 나뉘는데요. 이를 수렴할 때까지 반복하는 방식입니다. 동시에 해를 찾기 어려운 문제를 풀 때 많이 사용되는 방법론입니다. KC의 경우 (1) 각 군집 중심의 위치 (2)각 개체가 어떤 군집에 속해야 하는지 멤버십, 이 두 가지를 동시에 찾아야 하기 때문에 EM 알고리즘을 적용합니다.

군집 수 kk를 2로 정했습니다. 우선 군집의 중심(빨간색 점)을 랜덤 초기화합니다.

[![source: imgur.com](http://i.imgur.com/hgNzXsc.png)](http://imgur.com/hgNzXsc)

모든 개체들(파란색 점)을 아래 그림처럼 가장 가까운 중심에 군집(녹색 박스)으로 할당합니다. 이것이 Expectation 스텝입니다.

[![source: imgur.com](http://i.imgur.com/OFn22dM.png)](http://imgur.com/OFn22dM)

이번엔 중심을 군집 경계에 맞게 업데이트해 줍니다. 이것이 Maximization 스텝입니다.

[![source: imgur.com](http://i.imgur.com/UmBgHhf.png)](http://imgur.com/UmBgHhf)

다시 Expectation 스텝을 적용합니다. 바꿔 말해 모든 개체들을 가장 가까운 중심에 군집(보라색 박스)으로 할당해주는 작업입니다.

[![source: imgur.com](http://i.imgur.com/DWmbUxP.png)](http://imgur.com/DWmbUxP)

Maximization 스텝을 또 적용해 중심을 업데이트합니다. Expectation과 Maximization 스텝을 반복 적용해도 결과가 바뀌지 않거나(=해가 수렴), 사용자가 정한 반복수를 채우게 되면 학습이 끝납니다.

[![source: imgur.com](http://i.imgur.com/OBhtsbV.png)](http://imgur.com/OBhtsbV)

## KC의 특징과 단점

KC는 각 군집 중심의 초기값을 랜덤하게 정하는 알고리즘인데요. 아래 그림처럼 초기값 위치에 따라 원하는 결과가 나오지 않을 수도 있습니다.

[![source: imgur.com](http://i.imgur.com/jNTOXNQ.png)](http://imgur.com/jNTOXNQ)

군집의 크기가 다를 경우 제대로 작동하지 않을 수 있습니다.

[![source: imgur.com](http://i.imgur.com/IH8FAfq.png)](http://imgur.com/IH8FAfq)

군집의 밀도가 다를 경우에도 마찬가지입니다.

[![source: imgur.com](http://i.imgur.com/pJmhpQ6.png)](http://imgur.com/pJmhpQ6)

데이터 분포가 특이한 케이스도 군집이 잘 이루어지지 않습니다.

[![source: imgur.com](http://i.imgur.com/v37p0Gi.png)](http://imgur.com/v37p0Gi)

이 때문에 KC를 실제 문제에 적용할 때는 여러번 군집화를 수행해 가장 빈번히 등장하는 군집에 할당하는 **majority voting** 방법을 쓰는 경우가 많다고 합니다. KC의 계산복잡성은 O(n)O(n)으로 [계층적 군집화(Hiarchical clustering)](https://ratsgo.github.io/machine learning/2017/04/18/HC/)와 비교해 그리 무겁지 않은 알고리즘이기 때문입니다.





파이썬으로 한글 자연어를 처리하기 위해서는 java 기반으로 만들어진 형태소 분석기를 사용합니다.

**konlpy**는 자바 기반의 형태소 분석기를 파이썬에서 사용할 수 있게 해주는 아주 고마운 라이브러리입니다.



제공하는 형태소 분석기는 총 5개가 있습니다.

\1. jhannanum(한나눔) 

\2. kkma(꼬꼬마)

\3. komoran(코모란)

\4. twitter(트위터)

\5. mecab(메캅)





*konlpy 설명 : https://konlpy-ko.readthedocs.io/ko/v0.4.3/

*konlpy 분석기 별 성능 비교 : http://konlpy.org/ko/latest/morph/#comparison-between-pos-tagging-classes





형태소 분석 순서는 다음과 같습니다.



\1. 형태소 분석기 호출 

```
from konlpy.tag import Kkma
kkma = Kkma()
```



\2. 파싱함수 정의

```
def morph(input_data) : #형태소 분석
    preprocessed = kkma.pos(input_data)
    print(preprocessed)
```



\3. 문장 입력

```
morph("아메리카노는 맛있다.")
```



\4. 결과 확인

![img](https://t1.daumcdn.net/cfile/tistory/999FB3465BF0D5C327)





제 생각에는

**'아메리카노'  '는'  '맛있'  '다'  '.'** 

이렇게 파싱이 되어야 할 것 같은데 



현실은 

**'아메리카'  '노'  '는'  '맛있'  '다'  '.'** 

로 파싱이 되어버렸군요.





따라서 제 생각대로, 제 의도대로 분석이 되도록 하기 위해서는 **분석기의 사전을 수정**할 필요가 있습니다.



수정하는 방법은 다음과 같습니다. 



\1. konlpy내의 java폴더로 이동 

c드라이브에서 konlpy로 검색 및 폴더로 이동 > java 폴더로 이동

![img](https://t1.daumcdn.net/cfile/tistory/9947883E5BF0D73B29)







\2. kkma-2.0.jar 압축풀기

파일을 클릭하면 형태소 분석기 테스터가 실행됩니다.

따라서 압축풀기는 다음 그림처럼 파일 클릭 > 마우스 오른쪽 버튼 클릭 > 압축 풀기 로 하셔야 합니다.

![img](https://t1.daumcdn.net/cfile/tistory/99F46E415BF0D8530E)









\3. kkma-2.0 > dic 폴더로 이동 및 파일 확인 

압축을 풀고 나면 kkma-2.0이라는 폴더가 생성되어 있을 것입니다.

kkma-2.0 > dic 폴더로 이동하면 kkma에서 형태소 분석에 사용하는 사전들을 확인하실 수 있습니다.



3.1 사전 종류

![img](https://t1.daumcdn.net/cfile/tistory/99CA8F345BF0D9BE2A)



3.2 명사 사전

![img](https://t1.daumcdn.net/cfile/tistory/999669345BF0D9BE2B)









\4. 파일 내용 확인 및 사전 변경

![img](https://t1.daumcdn.net/cfile/tistory/9981483A5BF0DAB629)



사전에 아메리카노가 없었네요.

아메리카노가 온전히 **'아메리카노'** 로 분석되게 하기 위해 사전에 단어를 추가합니다.

 

![img](https://t1.daumcdn.net/cfile/tistory/99E8EF3A5BF0DBB52D)







\5. jar 파일 만들기

수정한 내용을 적용하기 위해서는 jar 파일로 만들고 기존의 kkma-2.0.jar과 바꿔줘야 합니다.



jar 파일로 만드는 방법은 다음과 같습니다.

1) 명령 프롬프트 실행

2) konlpy내의 java/kkma-2.0 폴더로 이동(예 : cd C:\Users\user\Anaconda3\Lib\site-packages\konlpy\java\kkma-2.0\

3) 명령어 : jar -cvf 생성하고자하는파일명.jar/ .

​        예 : jar -cvf user.jar/ .

![img](https://t1.daumcdn.net/cfile/tistory/992EC0345BF0EBD210)



kkma-2.0폴더에 user.jar 파일이 생성된 것을 확인하실 수 있습니다.





![img](https://t1.daumcdn.net/cfile/tistory/992A9F345BF0EBD310)







\6. 새로운 kkma-2.0.jar을 적용하기

user.jar 파일을 kkma-2.0.jar로 이름을 변경해주시면 됩니다.

문제가 생길 수 있으니 기존의 kkma-2.0.jar 파일은 백업해두시기를 권장드립니다.





\7. 결과 확인하기

7.1 라이브러리 재호출

```
from konlpy.tag import Kkma
kkma = Kkma()
```



7.2 문장 입력

```
morph("아메리카노는 맛있다.")
```



7.3 결과 확인

![img](https://t1.daumcdn.net/cfile/tistory/991F923D5BF0EB0006)

의도대로 수정이 된 것을 확인하실 수 있습니다.







Vue를 사용할 때 부모 컴포넌트에 있는 data를 자식 컴포넌트에서 필요로할 때가 있습니다(또는 넘겨줘야 할 경우가 있습니다). 그럴 경우 어떻게 부모 컴포넌트의 data를 자식 컴포넌트에게 넘겨줄 수 있을까요? 그럴 땐 props옵션을 사용하면 됩니다.

참고로 부모가 자식에게 data를 보낼 땐 props옵션을 사용하고 자식의 data를 부모에게 보내줄 땐 event를 이용합니다.

props옵션을 어떻게 사용하는지 알아보도록 하겠습니다.

props옵션을 정의하는 방법입니다. 아래는 Vue.js 공식문서(한글)에 예시로 나와있는 코드입니다. (예시는 모두 공식문서에 나와있는 코드를 사용하였습니다.)

```
Vue.component('child', {
  // props 정의
  props: ['message'],
  // 데이터와 마찬가지로 props는 템플릿 내부에서 사용할 수 있습니다
  // data 속성에서 사용할 땐 vm => ({vm.message})로 사용할 수 있습니다.
  template: '<span>{{ message }}</span>'
})
```

[컴포넌트 - Vue.js컴포넌트는 Vue의 가장 강력한 기능 중 하나입니다. 기본 HTML 엘리먼트를 확장하여 재사용 가능한 코드를 캡슐화하는 데 도움이 됩니다. 상위 수준에서 컴포넌트는 Vue의 컴파일러에 의해 동작이 추가된 사용자…kr.vuejs.org](https://kr.vuejs.org/v2/guide/components.html)

자식 컴포넌트에서 props: [‘message’]라고 정의한 부분의 의미는 자식이 부모 컴포넌트에게서 data를 받을 때 어디에다가 부모에게서 넘겨받은 해당 data를 넣을지를 정의하는 것입니다. 이제 부모에게서 해당 data를 받을 때(또는 부모가 자식에게 해당 data를 넘겨줄 때) 이 이름을 사용하면 됩니다.

그럼 ‘message’라고 정의해 놓았는데 부모 컴포넌트에선 자식 컴포넌트에 정의된 props를 어떻게 사용하는지를 봅시다.

```
<div>
  <child message="안녕하세요!"></child>
</div>
```

위와 같이 부모 컴포넌트에다가 정의한 자식 컴포넌트에 마치 HTML의 attribute를 사용하는 것과 같이 사용하면 됩니다. (정적 props)

만약 message라고 정의한 것이 아니라 myMessage(camelCase)라고 정의했다면 HTML 속성(attribute)은 대소문자를 구분하지 않기 때문에 그대로 사용해선 안되고 my-message(kebab-case)라고 사용해야 합니다.

```
<div>
  <child my-message="안녕하세요!"></child>
</div>
```

우선 간단하게 props를 사용하는 방법을 알아보았습니다. 예시로 보여드린 예제는 정적 props(Passing Static)인데요. 이 방법외에도 동적 props(Dynamic props)로 사용하는 방법이 있습니다.

아래는 동적 props를 사용하는 예시입니다.

```
<div>
  <input v-model="parentMsg">
  <br>
  <child v-bind:message="parentMsg"></child>
</div>
```

부모 컴포넌트에서 parentMsg를 입력(수정)할 때마다 변동되는 data가 props를 통해 자식에게 전달됩니다.

우리는 항상 정적인 data만 자식에게 넘겨주는 것이 아니로 동적으로 변하는 data를 자식에게 넘겨줘야 할 경우가 있습니다. 그럴 경우 동적 props를 사용하면 됩니다.

즉, 부모가 변동되지 않는 data를 자식에게 넘겨줄 땐 정적 props를 사용하고 부모에서 어떤 동작에 따라 변동되는 data를 넘겨줄 땐 동적 props를 사용하면 됩니다.