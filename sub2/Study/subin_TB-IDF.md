# TB-IDF

> **정보 검색**과 **텍스트 마이닝**에서 이용하는 가중치로, 여러 문서로 이루어진 문서군이 있을 때 어떤 단어가 특정 문서 내에서 얼마나 중요한 것인지를 나타내는 **통계적 수치**
>
> 문서의 핵심어를 추출하거나, 검색 엔진에서 검색 결과의 순위를 결정하거나, 문서들 사이의 비슷한 정도를 구하는 등의 용도로 사용할 수 있음

- TF

  > 특정한 단어가 문서 내에 얼마나 자주 등장하는지를 나타내는 값
  >
  > 이 값이 높을수록 문서에서 중요함
  >
  > ```markdown
  > TF (t) = (문서에 용어 t가 나타나는 횟수) / (문서에있는 총 용어 수)
  > ```

- DF

  > 단어 자체가 문서군 내에서 자주 사용되는 경우, 그 단어가 흔하게 등장한다는 것을 의미

- IDF

  > DF의 역수
  >
  > 문서군의 성격에 따라 결정됨
  >
  > ```markdown
  > IDF (t) = log_e (총 문서 수 / 용어가 t 인 문서 수)
  > ```

  `ex` **원자**

  일반적인 문서들 사이에서는 "원자"라는 낱말이 잘 나오지 않기 때문에 IDF 값이 높아지고 문서의 핵심어가 될 수 있지만, 이 단어에 대한 문서를 모아놓은 문서군의 경우 이 낱말은 상투어가 되어 각 문서들을 세분화하여 구분할 수 있는 다른 낱말들이 높은 가중치를 얻게 됨

- TB-IDF

  > TF와 IDF를 곱한 값
  >
  > 값이 클수록 용어가 드물고 중요하며, 그 반대도 그러함

- scikit-learn

  > Python에서 제공하는 라이브러리
  >
  > 각 문서 설명에 대한 TF-IDF 점수를 단어 단위로 계산 하는 사전 구축 된 TF-IDF 벡터 라이저를 제공함
  >
  > ```markdown
  > tf = TfidfVectorizer (분석기 = 'word', ngram_range = (1, 3), min_df = 0, stop_words = 'english')
  > tfidf_matrix = tf.fit_transform (ds [ '설명'])
  > ```

- 추천

  >```python
  >def item(id):   
  >	return ds.loc[ds['id'] == id][ 'description'].tolist()[0].split('-')[0]
  >
  ># 그냥 결과를 읽습니다. dictionary.def
  >
  >recommend(item_id, num): 
  >    print("Recommending" + str(num) + "products similar to" + item(item_id) + "...")    
  >    print("-------")
  >    recs = results[item_id][:num]    
  >    recs in recs: 
  >    	print("Recommended:" + item(rec[1]) + "(score: "+ str(rec[0]) + ")")
  >```
  >
  >`item_id` 원하는 권장 사항 수와 입력한 내용만 입력하면 됨
  >
  >`results[]`에 해당하는 것을 수집하고, `item_id` 화면에 권장 사항을 얻음
  >
  >```python
  >recommend(item_id = 11, num = 5)
  >```

###### 출처

> https://ko.wikipedia.org/wiki/Tf-idf
>
> https://heartbeat.fritz.ai/recommender-systems-with-python-part-i-content-based-filtering-5df4940bd831

