# KoNLPy 적용

```python
from konlpy.utils import pprint
from konlpy.tag import Kkma
from konlpy.tag import Hannanum
from konlpy.tag import Okt
from konlpy.tag import Komoran
from collections import Counter


@api_view(['GET'])
@permission_classes((AllowAny,))
def store_detail(request, id):
	store = get_object_or_404(models.Store, id=id)
    reviews = store.review_set.all()
    
    for i in range(len(reviews)):
        r = reviews[i].content
        nouns = Okt().nouns(r)
        count = Counter(nouns)
        tags = count.most_common(5)
        print(tags)
```

결과

```markdown
[('분위기', 1), ('수', 1), ('파스타', 1), ('맥주', 1), ('굿', 1)]
[('피자', 2), ('맛집', 2), ('파스타', 2), ('줄', 1), ('모두', 1)]
```

`print(r)`

![subin_KoNLPy적용_01](assets/subin_KoNLPy적용_01.png)

참고해야 할 문서

http://pearl.cs.pusan.ac.kr/~wiki/images/4/46/TR14-09-PBK-b.pdf

https://ericnjennifer.github.io/python_visualization/2018/01/21/PythonVisualization_Chapt3.html

[https://thinkwarelab.wordpress.com/2016/08/30/%ED%8C%8C%EC%9D%B4%EC%8D%AC-%ED%98%95%ED%83%9C%EC%86%8C-%EB%B6%84%EC%84%9D%EC%9C%BC%EB%A1%9C-%EC%9B%8C%EB%93%9C%ED%81%B4%EB%9D%BC%EC%9A%B0%EB%93%9C-%EA%B7%B8%EB%A6%AC%EA%B8%B0/](https://thinkwarelab.wordpress.com/2016/08/30/파이썬-형태소-분석으로-워드클라우드-그리기/)