# Sparse Matrix

> **희소 행렬**
>
> 매우 거대한 값들이 모여있는 행렬이지만 대부분이 0이다.

> **↔ 밀집 행렬(Dense Matrix)**
>
> 반대로 대부분이 1이다.

> 희소 행렬의 계산 효율을 위해 **곱셈, 역행렬** 등등 행렬 연산을 수행할 때, 별도의 알고리즘이 적용되어야 한다.
>
> `scipy.sparse`: 희소 행렬의 생성, 연산 등을 위한 함수, 객체를 제공
>
> `scipy.sparse.linalg`: 선형 대수 제공

- 참고 링크

  - http://scipy-lectures.org/advanced/scipy_sparse/storage_schemes.html

    > 영어로 되어있지만 자세한 설명이 되어 있음.

  - https://m.blog.naver.com/PostView.nhn?blogId=wideeyed&logNo=221182428460&proxyReferer=https%3A%2F%2Fwww.google.com%2F

    > 배열을 이용하여, sparse matrix 생성 예
    >
    > 데이터와 행, 열 인덱스로 sparse matrix 생성 예

  - [https://bkshin.tistory.com/entry/NLP-7-%ED%9D%AC%EC%86%8C-%ED%96%89%EB%A0%AC-Sparse-Matrix-COO-%ED%98%95%EC%8B%9D-CSR-%ED%98%95%EC%8B%9D](https://bkshin.tistory.com/entry/NLP-7-희소-행렬-Sparse-Matrix-COO-형식-CSR-형식)

    > 희소 행렬 - COO형식과 CSR형식

  - https://stackoverrun.com/ko/q/7196163

    > Huge sparse matrix in python

