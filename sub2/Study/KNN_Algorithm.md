# KNN_Algorithm

`KNN`은 `K-Nearest Neighbor`의 약자로, 최근접 이웃 알고리즘이라고도 한다.

이 알고리즘은 머신러닝 알고리즘들 중 가장 고전적이고 직관적이다.

`KNN`의 이론부터 `Python` 예제까지 다뤄볼 것이다.

## 이론

- KNN을 한마디로 표현하자면?

  > 유유상종
  >
  > 새로운 데이터를 입력 받았을 때 **가장 가까이 있는 것이 무엇이냐**를 중심으로 새로운 데이터의 종류를 정해주는 알고리즘이다.

- 물음표의 값을 유추해보자.

  ![image-20200403115725059](assets/image-20200403115725059.png)

  > 물음표에는 세모와 동그라미 중에 어떤 것이 들어갈까?
  >
  > 최근접 이웃 알고리즘은 주변에 있는 것과 같은 것이라고 판단하는 것이다.
  >
  > 따라서, 세모라고 판단했을 것이다.
  >
  > **하지만, 아래와 같은 경우라면 어떻게 판단할 수 있을까?**

- 물음표의 값을 다시 유추해보자.

  ![image-20200403120027728](assets/image-20200403120027728.png)

  > 물음표와 가장 가까운 것은 동그라미인데, 원을 확장해본다면 세모이다.
  >
  > 이러한 경우는 어떻게 판단하는 것일까?
  >
  > KNN은 단순하게 주변에 어떤 것이 가장 가까운가를 보는 것이 아니다.
  >
  > 주변에 있는 몇 개의 것들을 같이 보면서 가장 많은 것을 골라내는 방식이다.
  >
  > KNN에서 K는 주변의 개수를 의미한다.

- K의 값에 따라 물음표의 값이 달라진다.

  ![image-20200403120441525](assets/image-20200403120441525.png)

  > K = 1인 경우, 파란 동그라미로 판단할 수 있다.
  >
  > K = 4인 경우, 빨간 세모로 판단할 수 있다.
  >
  > 도대체 K가 몇이여야 좋은 것인가?
  >
  > 최선의 K 값을 선택하는 것은 데이터마다 다르게 접근해야한다. 일반적으로 K의 값이 커질 수록 분류에서 이상치의 영향이 줄어드나, 분류 자체를 못하게 되는 상황이 발생한다. 그래서 **총 데이터의 제곱근 값을 사용**하고 있고, 최적의 K 값을 찾기 위한 연구는 진행 중이다.

- 참고 링크

  > https://gomguard.tistory.com/51
  >
  > 예제 1(토마토 분류)과 예제 2(암 진단)도 있으니 한 번 읽어보는 것이 좋겠다.

<br/>

## `Python` 예제 1

> KNN을 Python으로 총 3단계를 걸쳐 설명하였다.
>
> Numpy, Pandas는 사용하지 않았다.

- 거리 계산하기

  > 유클리드 거리 공식을 사용한다.

- 가장 근처에 있는 요소 뽑기

- 예측하기

- 참고 링크

  > https://firework-ham.tistory.com/27

## `Python` 예제 2

> K는 과연 몇이어야 할까?
>
> 그룹이 2개라면, K는 홀수 값을 주는 것이 맞다.
>
> 그룹이 3개라면, K가 3이어서는 안된다.
>
> 분류될 그룹의 종류 등을 고려하여 K값을 설정하여야 한다.
>
> Numpy, Pandas, Matplotlib.pyplot을 사용하였다.

- 참고 링크

  > https://doorbw.tistory.com/175
  >
  > 파이썬 코드 Github은 막혔다.

## `Python` 예제 3

> Sklearn(싸이킷런)을 사용하였다.

- 참고 링크

  > http://hleecaster.com/ml-knn-classifier-example/
  >
  > 데이터 프레임 생성법
  >
  > http://hleecaster.com/python-pandas-creating-and-loading-dataframes/

## `Python` 예제 4

> Numpy, Pandas, Warning을 사용하였다.
>
> CSV파일을 불러와 사용하였다.

- 참고 링크

  > https://superkong1.tistory.com/24

## `Python` 예제 5

> Numpy를 사용하였다.

- 참고 링크

  > https://doongkibangki.tistory.com/16