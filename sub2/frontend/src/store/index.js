import Vue from "vue";
import Vuex from "vuex";
import data from "./modules/data";
import app from "./modules/app";
import auth from "./modules/auth";

import createPersistedState from "vuex-persistedstate";


Vue.use(Vuex);

const plugins = [
  createPersistedState({
    storage: window.sessionStorage,
    paths: ["auth"]
  })
]

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    data,
    app,
    auth
  },
  plugins: plugins,
});
