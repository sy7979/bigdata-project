import axios from "axios";

export default axios.create({
  baseURL: "http://i02c107.p.ssafy.io:8000",
  // baseURL: "http://127.0.0.1:8000",
  headers: {
    "Content-type": "application/json",
  }
});
