export default [
  {
    path: "",
    view: "Home",
    name: "home"
  },
  {
    path: "/search",
    view: "Search",
    name: "search"
  },
  {
    path: "/detail/:id",
    view: "Detail",
    name: "detail"
  },
  {
    path: "/signin",
    view: "Signin",
    name: "signin"
  },
  {
    path: "/signup",
    view: "Signup",
    name: "Signup"
  },
  {
    path: "/storedetail/:id",
    view: "StoreDetail",
    name: "StoreDetail"
  },
  {
    path: "/listpage",
    view: "ListPage",
    name: "ListPage"
  },
  {
    path: "/mypage",
    view: "Mypage",
    name: "mypage"
  },
  {
    path: "/myinfoupdate",
    view: "MyInfoUpdate",
    name: "myinfoupdate"
  },
  {
    path: "/recommend",
    view: "Recommend",
    name: "recommend"
  },
  {
    path: "/logout",
    view: "Logout",
    name: "logout"
  },
  {
    path: "/topreview",
    view: 'TopReview',
    name: "topreview"
  },
  {
    path: "/topscore",
    view: "TopScore",
    name: "topscore"
  }
];
