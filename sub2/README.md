# README

## axios 경로

### 회원 관련

**회원가입 (POST)**

`/rest-auth/registration/`

**로그인 (POST)**

`/rest-auth/login/`

**로그아웃 (POST)**

`/rest-auth/logout/`



### 유저 페이지

**회원정보 페이지 (GET)**

`/api/users/{user_id}/`

**회원정보 수정 (PUT)**

`/api/userinfos/`



### 음식점

**음식점 리스트 (GET)**

`/api/stores`

**음식점 정보 (GET)**

`/api/stores/{store_id}/`



### 리뷰

**리뷰 생성 (POST)**

`/api/reviews/`

**리뷰 수정 (PUT)**

`/api/reviews/{review_id}/`

**리뷰 삭제 (DELETE)**

`/api/reviews/{review_id}/`



### 검색

**검색 (GET)**

`/api/search/`



### 추천

**음식점 추천 (GET)**

`/api/recommend/`

