from django.utils import timezone
from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.conf import settings

from django.db.models.signals import post_save
from django.dispatch import receiver


class Store(models.Model):
    id = models.IntegerField(primary_key=True) # primary_key가 True라면 key가 자동으로 들어가지 않음
    store_name = models.CharField(max_length=50)
    branch = models.CharField(max_length=20, null=True)
    area = models.CharField(max_length=50, null=True)
    tel = models.CharField(max_length=20, null=True)
    address = models.CharField(max_length=200, null=True)
    latitude = models.FloatField(max_length=10, null=True)
    longitude = models.FloatField(max_length=10, null=True)
    category = models.CharField(max_length=200, null=True)

    @property
    def category_list(self):
        return self.category.split("|") if self.category else []

    def __str__(self):
        if self.area != None:
            return f'{self.store_name} ({self.area})'
        else:
            return f'{self.store_name} ({self.address})'


class User(AbstractUser):
    gender = models.CharField(max_length=10, blank=True, null=True)
    age = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f'{self.username}'


class Menu(models.Model):
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    menu_name = models.CharField(max_length=50, null=True)
    price = models.CharField(max_length=50, null=True)

    def __str__(self):
        return f'{self.store} - {self.menu_name}'


class Review(models.Model):
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    score = models.IntegerField()
    content = models.CharField(max_length=200, null=True)
    # reg_time = models.DateTimeField()
    reg_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.store} - {self.user} - {self.score}점'


class Prescription(models.Model):
    name = models.CharField(max_length=100, null=True)
    symptom = models.CharField(max_length=200, null=True)
    ingredient = models.CharField(max_length=100, null=True)
    care = models.CharField(max_length=500, null=True)

    def __str__(self):
        return f'{self.name} - {self.ingredient}'


class StoreImage(models.Model):
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    img_1 = models.CharField(max_length=10000, null=True)
    img_2 = models.CharField(max_length=10000, null=True)
    img_3 = models.CharField(max_length=10000, null=True)
    
    def __str__(self):
        return f'{self.store}의 이미지'