from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from api import views
from django.urls import path, include


router = DefaultRouter(trailing_slash=False)
router.register(r"", views.StoreViewSet, basename="stores")

urlpatterns = router.urls

urlpatterns = [
    path("users/<int:id>/", views.user_page), # 유저 정보 (유저, 리뷰)
    path("userinfos/", views.user_info), # 유저 정보 수정 (성별, 나이)
    path("stores", include(router.urls)), # 모든 음식점 목록
    path("stores/<int:id>/", views.store_detail), # 음식점 목록 (음식점, 이미지, 메뉴, 리뷰)
    path("reviews/", views.reviews), # 리뷰 생성
    path("reviews/<int:id>/", views.review_detail), # 리뷰 수정, 삭제
    path("search/", views.search), # 검색
    path("recommend/", views.recommend_store), # 음식점 추천
    path("prescriptions/delete/", views.prescriptions_remove), # 처방전 삭제 (관리자만 가능)
    path("keywords/", views.keywords), # 키워드
    path("scores/", views.store_score), # 평점순
    path("crawling/", views.crawling), # 크롤링
    path("addnullimage/", views.addnullimage), # 이미지 빈값 넣기
    # path("test/", views.test),
]