from api import models, serializers
from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination
from django.shortcuts import get_object_or_404
from django.http import JsonResponse, HttpResponse
from django.contrib.auth import get_user_model
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser, IsAuthenticatedOrReadOnly
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

import requests
import urllib.request
from bs4 import BeautifulSoup

import math
from django.db.models import Avg, Count

# 자연어처리
from konlpy.tag import Okt
from collections import Counter

# 크롤링
from selenium import webdriver
from bs4 import BeautifulSoup
import time
import json


class SmallPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = "page_size"
    max_page_size = 50


@permission_classes((AllowAny,))
class StoreViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.StoreSerializer
    pagination_class = SmallPagination

    def get_queryset(self):
        name = self.request.query_params.get("name", "")
        queryset = (
            models.Store.objects.all().filter(store_name__contains=name).order_by("id")
        )
        return queryset


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
@authentication_classes((JSONWebTokenAuthentication,))
def user_page(request, id):
    if request.user.id == id:
        user = get_object_or_404(get_user_model(), id=id)
        serializer = serializers.UserSerializer(user)

        reviews = user.review_set.all()
        serializer2 = serializers.UserReviewSerializer(reviews, many=True)

        context = {
            "data": serializer.data,
            "reviews": serializer2.data
        }
        return JsonResponse(context)
    return HttpResponse(status=400)


@api_view(['PUT'])
@permission_classes((IsAuthenticated,))
@authentication_classes((JSONWebTokenAuthentication,))
def user_info(request):
    user_id = request.user.id
    user = get_object_or_404(get_user_model(), id=user_id)
    if len(request.data["gender"]) == 0 or len(request.data["age"]) == 0:
        return JsonResponse({"message": "회원정보를 입력해주세요."})
    if int(request.data["age"]) <= 0 or int(request.data["age"]) > 120:
        return JsonResponse({"message": "나이를 확인해주세요."})
    if request.data["gender"] not in ["남", "여"]:
        return JsonResponse({"message": "성별을 확인해주세요."})
    serializer = serializers.UserInfoSerializer(user, data=request.data)
    if serializer.is_valid(raise_exception=True):
        serializer.save()
        return JsonResponse(serializer.data)
    return HttpResponse(status=400)


@api_view(['GET'])
@permission_classes((AllowAny,))
def store_detail(request, id):
    store = get_object_or_404(models.Store, id=id)
    serializer = serializers.StoreSerializer(store)

    store_image_set = get_object_or_404(models.StoreImage, id=id)
    serializer2 = serializers.OnlyStoreImageSerializer(store_image_set)

    menus = store.menu_set.all()
    serializer3 = serializers.StoreMenuSerializer(menus, many=True)

    # 리뷰 가져오기
    reviews = store.review_set.all().order_by("-id") # store.review_set.all(): 해당되는 store의 모든 review를 가져옴
    serializer4 = serializers.StoreReviewSerializer(reviews, many=True)
    context = {

        "data": serializer.data,
        "store_image_set": serializer2.data,
        "menus": serializer3.data,
        "reviews": serializer4.data
    }
    return JsonResponse(context)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@authentication_classes((JSONWebTokenAuthentication,))
def reviews(request):
    # front에서 store_id로 보내줄 경우
    store_id = request.data["store_id"]
    # store_id = 1
    store = get_object_or_404(models.Store, id=store_id)
    
    user_id = request.user.id
    user = get_object_or_404(get_user_model(), id=user_id)
    
    if user.gender != None and user.age != None:
        serializer = serializers.ReviewCreateSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            reviews = serializer.save(store=store, user=user)
            serializer2 = serializers.ReviewSerializer(reviews)
            return JsonResponse(serializer2.data)
        return HttpResponse(status=400)
    return JsonResponse({"message": "회원정보의 성별과 나이를 추가해주세요."})


@api_view(['PUT', 'DELETE'])
@permission_classes((IsAuthenticated,))
@authentication_classes((JSONWebTokenAuthentication,))
def review_detail(request, id):
    review = get_object_or_404(models.Review, id=id)
    if request.method == "DELETE":
        review.delete()
        return HttpResponse(status=204)

    elif request.method == "PUT":
        serializer = serializers.ReviewCreateSerializer(review, data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return JsonResponse(serializer.data)
        return HttpResponse(status=400)


@api_view(['POST'])
@permission_classes((AllowAny,))
def search(request):
    store_name = request.data["store_name"]
    local = request.data["local"]

    # store_name이 비어있거나 local이 비어있는 경우
    if store_name != "" or local != "":
        stores = models.Store.objects.filter(store_name__icontains=store_name).filter(address__icontains=local)
        if len(stores) >= 10:
            stores = stores[:10]
        if len(stores) == 0:
            return JsonResponse({"message": "검색 결과가 없습니다."})
        serializer = serializers.SearchStoreSerializer(stores, many=True)
        return JsonResponse(serializer.data, safe=False)
    return JsonResponse({"message": "항목을 입력해주세요"})


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@authentication_classes((JSONWebTokenAuthentication,))
def recommend_store(request):
    user_id = request.user.id
    user = get_object_or_404(get_user_model(), id=user_id)
    my_reviews = user.review_set.all()
    menus = models.Menu.objects.all()

    health = request.data["health"]
    user_location = request.data["local"]

    # # 삭제해야함
    # health = ["이질", "감기초기"]
    # user_location = "광주"

    health_stores = []
    health_users = []
    if health != []:
        health_ingredient_dict = {}
        health_store_dict = {}
        health_user_dict = {}

        # health에 해당하는 모든 prescriptions 가져오기
        prescriptions = models.Prescription.objects.filter(name__in=health)

        for p in prescriptions:
            ingredient = p.ingredient
            
            try:
                if health_ingredient_dict[ingredient]:
                    pass
            except:
                health_ingredient_dict[ingredient] = True
                # user_location과 ingredient로 필터링
                health_menus = menus.filter(store__address__icontains=user_location).filter(menu_name__icontains=ingredient)

                # 한글자 재료 처리 (파, 무, 꿀)
                if len(ingredient) == 1:
                    if ingredient == "파":
                        health_menus = health_menus.exclude(
                                                    menu_name__icontains="스파"
                                                ).exclude(
                                                    menu_name__icontains="양파"
                                                ).exclude(
                                                    menu_name__icontains="엠파"
                                                ).exclude(
                                                    menu_name__icontains="짜파"
                                                ).exclude(
                                                    menu_name__icontains="콘파"
                                                ).exclude(
                                                    menu_name__icontains="파나"
                                                ).exclude(
                                                    menu_name__icontains="파니"
                                                ).exclude(
                                                    menu_name__icontains="파블"
                                                ).exclude(
                                                    menu_name__icontains="파스"
                                                ).exclude(
                                                    menu_name__icontains="파운"
                                                ).exclude(
                                                    menu_name__icontains="파울"
                                                ).exclude(
                                                    menu_name__icontains="파이"
                                                ).exclude(
                                                    menu_name__icontains="파인"
                                                ).exclude(
                                                    menu_name__icontains="파쵸"
                                                ).exclude(
                                                    menu_name__icontains="파치"
                                                ).exclude(
                                                    menu_name__icontains="파티"
                                                ).exclude(
                                                    menu_name__icontains="파프"
                                                ).exclude(
                                                    menu_name__icontains="파히"
                                                ).exclude(
                                                    menu_name__icontains="파마"
                                                ).exclude(
                                                    menu_name__icontains="페파"
                                                ).exclude(
                                                    menu_name__icontains="파블"
                                                ).exclude(
                                                    menu_name__icontains="파파"
                                                )
                                                
                    if ingredient == "무":
                        health_menus = health_menus.exclude(
                                                    menu_name__icontains="가무"
                                                ).exclude(
                                                    menu_name__icontains="각무"
                                                ).exclude(
                                                    menu_name__icontains="나무"
                                                ).exclude(
                                                    menu_name__icontains="무네"
                                                ).exclude(
                                                    menu_name__icontains="무등"
                                                ).exclude(
                                                    menu_name__icontains="무료"
                                                ).exclude(
                                                    menu_name__icontains="무릎"
                                                ).exclude(
                                                    menu_name__icontains="무뼈"
                                                ).exclude(
                                                    menu_name__icontains="무사"
                                                ).exclude(
                                                    menu_name__icontains="무스"
                                                ).exclude(
                                                    menu_name__icontains="무알"
                                                ).exclude(
                                                    menu_name__icontains="무제"
                                                ).exclude(
                                                    menu_name__icontains="무침"
                                                ).exclude(
                                                    menu_name__icontains="무한"
                                                ).exclude(
                                                    menu_name__icontains="무화"
                                                ).exclude(
                                                    menu_name__icontains="스무"
                                                ).exclude(
                                                    menu_name__icontains="휴무"
                                                ).exclude(
                                                    menu_name__icontains="무슬"
                                                ).exclude(
                                                    menu_name__icontains="충무"
                                                ).exclude(
                                                    menu_name__icontains="감나무"
                                                ).exclude(
                                                    menu_name__icontains="무지개"
                                                )

                    if ingredient == "꿀":
                        health_menus = health_menus.exclude(
                                                    menu_name__icontains="꿀/"
                                                ).exclude(
                                                    menu_name__icontains="꿀꺽"
                                                ).exclude(
                                                    menu_name__icontains="꿀꿀"
                                                ).exclude(
                                                    menu_name__icontains="꿀맛"
                                                ).exclude(
                                                    menu_name__icontains="꿀모"
                                                ).exclude(
                                                    menu_name__icontains="꿀삼"
                                                )
                
                for m in health_menus:
                    # 각각의 메뉴에서 store 가져오기
                    health_store = m.store

                    # store review들의 score 평균을 구함
                    store_average_dict = health_store.review_set.aggregate(Avg("score"))
                    store_average_score = store_average_dict["score__avg"]
                    if store_average_score != None and store_average_score > 0:
                        # store 중복체크 진행
                        try:
                            if health_store_dict[health_store.id]:
                                pass
                        except:
                            health_store_dict[health_store.id] = True
                            # 각각의 store에서 review들을 가져옴
                            reviews = health_store.review_set.all()
                            if len(reviews) != 0:
                                health_stores.append([health_store, m, p])
                                for r in reviews:
                                    # 각 user에서 가져온 user의 중복체크 진행
                                    try:
                                        if health_user_dict[r.user.username]:
                                            pass
                                    except:
                                        health_user_dict[r.user.username] = True
                                        if r.user != request.user:
                                            # health_store에 리뷰를 작성한 user
                                            health_users.append(r.user)

    if len(my_reviews) != 0:

        my_score_mean = 0 # 나의 평균 평점
        for my_review in my_reviews:
            my_score_mean += my_review.score
        my_score_mean /= len(my_reviews)
        my_score_mean = round(my_score_mean, 2)

        result_user = -1
        result_min = 1000000000

        # 건강에 관한 음식점이 없을 경우
        if len(health_stores) == 0:
            # 다른 유저 전부 가져옴
            other_users = models.User.objects.all().exclude(id=user_id)

            for o in other_users:
                # review들을 user_location으로 필터링
                o_reviews = o.review_set.all().filter(store__address__icontains=user_location)
                if len(o_reviews) != 0:
                    o_score_mean = 0 # 다른 사람의 평균 평점
                    for o_review in o_reviews:
                        o_score_mean += o_review.score
                    o_score_mean /= len(o_reviews)
                    o_score_mean = round(o_score_mean, 2)
                    
                    # 유사도 구하기
                    similarity = math.sqrt(
                        (o_score_mean - my_score_mean)**2 + (o.age - user.age)**2
                    )
                    if result_min > similarity:
                        result_user = o
                        result_min = similarity
            # 유사한 유저가 없다면
            if result_user == -1:
                return JsonResponse({"message": "검색 결과가 없습니다."})
            
            # 평점 순으로 정렬
            result_store = result_user.review_set.all().filter(store__address__icontains=user_location).order_by("-score")[0].store
            serializer = serializers.StoreSerializer(result_store)
            context = {
                "store": serializer.data,
                "message": "유사도",
            }
            return JsonResponse(context)

        # 추천 재료 외 다른 추천 리스트
        other_store_list = []
        count = 0
        other_ingredient_dict = {}
        for os in health_stores:
            other_store_dict = {}
            try:
                if other_ingredient_dict[os[2]]:
                    pass
            except:
                other_ingredient_dict[os[2]] = True
                count += 1
                other_store_dict["other_store"] = {
                    "id": os[0].id,
                    "store_name": os[0].store_name,
                    "branch": os[0].branch,
                    "area": os[0].area,
                    "tel": os[0].tel,
                    "address": os[0].address,
                    "latitude": os[0].latitude,
                    "longitude": os[0].longitude,
                    "category_list": os[0].category_list,
                }
                other_store_dict["other_menu"] = {
                    "id": os[1].id,
                    "menu_name": os[1].menu_name,
                    "price": os[1].price,
                }
                other_store_dict["other_prescription"] = {
                    "name": os[2].name,
                    "symptom": os[2].symptom,
                    "ingredient": os[2].ingredient,
                    "care": os[2].care,
                }
                other_store_list.append(other_store_dict)
            if count == 10:
                break
        
        for u in health_users:
            # health_stores에 평점을 단 user의 모든 리뷰
            u_reviews = u.review_set.all()
            u_score_mean = 0
            for u_review in u_reviews:
                u_score_mean += u_review.score
            u_score_mean /= len(u_reviews)
            u_score_mean = round(u_score_mean, 2)
            similarity = math.sqrt(
                (u_score_mean - my_score_mean)**2 + (u.age - user.age)**2
            )
            if result_min > similarity:
                result_user = u
                result_min = similarity
        
        max_score = -1
        result_store = -1
        result_prescription = -1
        result_menu = -1
        for s in health_stores:
            # 각각의 store에 가장 유사한 user를 기준으로 review를 필터링
            result_user_review = s[0].review_set.filter(user=result_user)
            if len(result_user_review) != 0:
                result_user_review = result_user_review.order_by("-score")[0]
                # 만약 지금까지의 score 중 가장 높은 평점이라면
                if result_user_review.score > max_score:
                    # 저장
                    max_score = result_user_review.score
                    result_store = result_user_review.store
                    result_menu = s[1]
                    result_prescription = s[2]

        serializer = serializers.StoreSerializer(result_store)
        serializer2 = serializers.StoreMenuSerializer(result_menu)
        serializer3 = serializers.PrescriptionSerializer(result_prescription)
        
        context = {
            "store": serializer.data,
            "menu": serializer2.data,
            "prescription": serializer3.data,
            "other_store_list": other_store_list,
            "message": "건강추천시스템",
        }
        return JsonResponse(context)
                
    return JsonResponse({"message": "리뷰 작성이 필요합니다."})


@api_view(['DELETE'])
@permission_classes((IsAdminUser,))
@authentication_classes((JSONWebTokenAuthentication,))
def prescriptions_remove(request):
    health = models.Prescription.objects.all()
    menus = models.Menu.objects.all()

    if health != []:
        health_dict = {}
        ingredients = []
        for h in health:
            prescriptions = models.Prescription.objects.all().filter(name__icontains=h.name)

            for p in prescriptions:
                ingredient = p.ingredient
                try:
                    if health_dict[ingredient]:
                        pass
                except:
                    health_dict[ingredient] = True

                    # if len(ingredient) == 1:
                    #     print(ingredient)

                    health_menu = menus.filter(menu_name__icontains=ingredient)
                    if len(health_menu) == 0:
                        ingredients.append(ingredient)

        for i in ingredients:
            de = models.Prescription.objects.all().filter(ingredient=i)
            for d in de:
                d.delete()


@api_view(['POST'])
@permission_classes((AllowAny,))
def keywords(request):
    
    user_location = request.data["local"]

    # # 삭제해야함
    # user_location = "서울"

    stores = models.Store.objects.filter(address__icontains=user_location).annotate(review_cnt=Count("review")).order_by("-review_cnt").filter(review_cnt__gte=10)[:10]
    store_keywords_list = []
    for store in stores:
        reviews = store.review_set.all()

        store_keywords = {}
        texts = []
        okt = Okt()
        for r in reviews:
            text_group = okt.nouns(r.content)
            texts += text_group

        count = Counter(texts)
        words = dict(count.most_common(10))
        keywords = list(words.keys())

        serializer = serializers.StoreSerializer(store)

        store_keywords["store"] = serializer.data
        store_keywords["keywords"] = keywords

        store_keywords_list.append(store_keywords)

    context = {
        "store_keywords_list": store_keywords_list,
    }

    return JsonResponse(context)


@api_view(['POST'])
@permission_classes((AllowAny,))
def store_score(request):

    user_location = request.data["local"]

    # # 삭제해야함
    # user_location = "서울"

    stores = models.Store.objects.filter(address__icontains=user_location).annotate(review_cnt=Count("review")).filter(review_cnt__gte=10).annotate(avg_score=Avg("review__score")).order_by("-avg_score")[:10]
    store_scores_list = []
    for store in stores:
        reviews = store.review_set.all()

        store_keywords = {}
        texts = []
        okt = Okt()
        for r in reviews:
            text_group = okt.nouns(r.content)
            texts += text_group

        count = Counter(texts)
        words = dict(count.most_common(10))
        keywords = list(words.keys())

        serializer = serializers.StoreSerializer(store)

        store_keywords["store"] = serializer.data
        store_keywords["keywords"] = keywords

        store_scores_list.append(store_keywords)

    context = {
        "store_scores_list": store_scores_list,
    }

    return JsonResponse(context)


@api_view(['GET'])
@permission_classes((IsAdminUser,))
@authentication_classes((JSONWebTokenAuthentication,))
def crawling(request):
    stores = models.Store.objects.all()[:100]

    try:
        with open("../../data/data3.json", "r", encoding="utf-8") as f:
            store_dict_list = json.load(f)
    except:
        store_dict_list = []

    for store in stores:
        # print("+++++++++++++++", store.id, store.store_name, "++++++++++++++++")
        store_dict = {}
        store_dict["id"] = store.id
        
        # 검색 크롤링
        search_url = "https://www.diningcode.com/isearch.php?query="
        store_name = store.store_name
        search_url += store_name

        driver = webdriver.Chrome(executable_path="../../data/chromedriver.exe")
        driver.get(search_url)
        store_response = driver.page_source
        store_soup = BeautifulSoup(store_response, 'html.parser')
        stores = store_soup.select("#div_rn > ul > li:nth-child(2) > a")

        time.sleep(1)
        if len(stores) != 0:
            store_url = stores[0].get("href")

            store_detail_url = "https://www.diningcode.com" + store_url

            driver.get(store_detail_url)
            store_image_response = driver.page_source
            driver.quit()
            store_image_soup = BeautifulSoup(store_image_response, "html.parser")
            store_images = store_image_soup.select("#div_profile > div.s-list.pic-grade > ul")
            if len(store_images) != 0:
                store_images_url = store_images[0].find_all("img")

                if len(store_images_url) >= 3:
                    store_dict["img_1"] = store_images_url[0].get("src")
                    store_dict["img_2"] = store_images_url[1].get("src")
                    store_dict["img_3"] = store_images_url[2].get("src")
                elif len(store_images_url) == 2:
                    store_dict["img_1"] = store_images_url[0].get("src")
                    store_dict["img_2"] = store_images_url[1].get("src")
                    store_dict["img_3"] = ""
                elif len(store_images_url) == 1:
                    store_dict["img_1"] = store_images_url[0].get("src")
                    store_dict["img_2"] = ""
                    store_dict["img_3"] = ""
            else:
                store_dict["img_1"] = ""
                store_dict["img_2"] = ""
                store_dict["img_3"] = ""
        else:
            store_dict["img_1"] = ""
            store_dict["img_2"] = ""
            store_dict["img_3"] = ""

        store_dict_list.append(store_dict)
    # print(store_dict_list)
    with open("../../data/data3.json", "w", encoding="utf-8") as f:
        json.dump(store_dict_list, f, ensure_ascii=False, indent="\t")

    return JsonResponse({"message": "success"})


@api_view(['GET'])
@permission_classes((IsAdminUser,))
@authentication_classes((JSONWebTokenAuthentication,))
def addnullimage(request):
    stores = models.Store.objects.all()[100:]

    try:
        with open("../../data/data3.json", "r", encoding="utf-8") as f:
            store_dict_list = json.load(f)
    except:
        store_dict_list = []

    for store in stores:
        store_dict = {}

        store_dict["id"] = store.id
        store_dict["img_1"] = ""
        store_dict["img_2"] = ""
        store_dict["img_3"] = ""

        store_dict_list.append(store_dict)
    
    with open("../../data/data3.json", "w", encoding="utf-8") as f:
        json.dump(store_dict_list, f, ensure_ascii=False, indent="\t")

    return JsonResponse({"message": "success"})


# @api_view(['GET'])
# @permission_classes((AllowAny,))
# def test(request):
#     store_image = get_object_or_404(models.StoreImage, id=1)
#     serializer = serializers.OnlyStoreImageSerializer(store_image)
#     return JsonResponse(serializer.data)