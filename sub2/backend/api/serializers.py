from .models import Store, User, Menu, Review, Prescription, StoreImage
from rest_framework import serializers


class StoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = [
            "id",
            "store_name",
            "branch",
            "area",
            "tel",
            "address",
            "latitude",
            "longitude",
            "category_list",
        ]


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "id",
            "username",
            "gender",
            "age",
        ]


class UserInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "gender",
            "age",
        ]


class MenuSerializer(serializers.ModelSerializer):
    store = StoreSerializer()

    class Meta:
        model = Menu
        fields = [
            "id",
            "store",
            "menu_name",
            "price",
        ]


class ReviewSerializer(serializers.ModelSerializer):
    store = StoreSerializer()
    user = UserSerializer()

    class Meta:
        model = Review
        fields = [
            "id",
            "store",
            "user",
            "score",
            "content",
            "reg_time",
        ]


class ReviewCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Review
        fields = [
            "id",
            "score",
            "content",
            "reg_time",
        ]


class StoreReviewSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Review
        fields = [
            "id",
            "user",
            "score",
            "content",
            "reg_time",
        ]


class UserReviewSerializer(serializers.ModelSerializer):
    store = StoreSerializer()

    class Meta:
        model = Review
        fields = [
            "id",
            "store",
            "score",
            "content",
            "reg_time",
        ]


class StoreMenuSerializer(serializers.ModelSerializer):

    class Meta:
        model = Menu
        fields = [
            "id",
            "menu_name",
            "price",
        ]


class SearchStoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = [
            "id",
            "store_name",
            "branch",
            "area",
            "tel",
            "address",
            "latitude",
            "longitude",
            "category_list",
        ]


class PrescriptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Prescription
        fields = [
            "name",
            "symptom",
            "ingredient",
            "care",
        ]


class StoreImageSerializer(serializers.ModelSerializer):
    store = StoreSerializer()

    class Meta:
        model = StoreImage
        fields = [
            "store",
            "img_1",
            "img_2",
            "img_3",
        ]


class OnlyStoreImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = StoreImage
        fields = [
            "img_1",
            "img_2",
            "img_3",
        ]