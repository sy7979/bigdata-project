from pathlib import Path
import pandas as pd
from django.core.management.base import BaseCommand
from backend import settings
from api import models


class Command(BaseCommand):
    help = "initialize database"
    DATA_DIR = Path(settings.BASE_DIR).parent.parent / "data"
    DATA_FILE = str(DATA_DIR / "dump.pkl")
    DATA_FILE_2 = str(DATA_DIR / "dump2.pkl")
    DATA_FILE_3 = str(DATA_DIR / "dump3.pkl")

    def _load_dataframes(self):
        try:
            data = pd.read_pickle(Command.DATA_FILE)
            data2 = pd.read_pickle(Command.DATA_FILE_2)
            data3 = pd.read_pickle(Command.DATA_FILE_3)
        except:
            print(f"[-] Reading {Command.DATA_FILE} failed")
            print(f"[-] Reading {Command.DATA_FILE_2} failed")
            print(f"[-] Reading {Command.DATA_FILE_3} failed")
            exit(1)
        return data, data2, data3

    def _initialize(self):
        """
        Sub PJT 1에서 만든 Dataframe을 이용하여 DB를 초기화합니다.
        """
        print("[*] Loading data...")
        dataframes, dataframes2, dataframes3 = self._load_dataframes()

        print("[*] Initializing stores...")
        models.Store.objects.all().delete()
        stores = dataframes["stores"]
        stores_bulk = [
            models.Store(
                id=store.id,
                store_name=store.store_name,
                branch=store.branch,
                area=store.area,
                tel=store.tel,
                address=store.address,
                latitude=store.latitude,
                longitude=store.longitude,
                category=store.category,
            )
            for store in stores.itertuples()
        ]
        models.Store.objects.bulk_create(stores_bulk)
        print("[+] Done")


        print("[*] Initializing users...")
        models.User.objects.all().delete()
        users = dataframes["users"]
        users_bulk = [
            models.User(
                username=user.username,
                gender=user.gender,
                age=user.age,
            )
            for user in users.itertuples()
        ]
        models.User.objects.bulk_create(users_bulk)
        print("[+] Done")


        print("[*] Initializing review...")
        models.Review.objects.all().delete()
        reviews = dataframes["reviews"]
        reviews_bulk = [
            models.Review(
                store=models.Store.objects.get(id=review.store),
                user=models.User.objects.get(username=review.user),
                score=review.score,
                content=review.content,
                reg_time=review.reg_time,
            )
            for review in reviews.itertuples()
        ]
        models.Review.objects.bulk_create(reviews_bulk)
        print("[+] Done")


        print("[*] Initializing menu...")
        models.Menu.objects.all().delete()
        menus = dataframes["menus"]
        menus_bulk = [
            models.Menu(
                store=models.Store.objects.get(id=menu.store),
                menu_name=menu.menu,
                price=menu.price,
            )
            for menu in menus.itertuples()
        ]
        models.Menu.objects.bulk_create(menus_bulk)
        print("[+] Done")

        print("[*] Initializing prescription...")
        models.Prescription.objects.all().delete()
        prescriptions = dataframes2["prescriptions"]
        prescriptions_bulk = [
            models.Prescription(
                name=prescription.name,
                symptom=prescription.symptom,
                ingredient=prescription.ingredient,
                care=prescription.care,
            )
            for prescription in prescriptions.itertuples()
        ]
        models.Prescription.objects.bulk_create(prescriptions_bulk)
        print("[+] Done")


        print("[*] Initializing storeimage...")
        models.StoreImage.objects.all().delete()
        storeimages = dataframes3["store_images"]
        storeimages_bulk = [
            models.StoreImage(
                store=models.Store.objects.get(id=storeimage.id),
                img_1=storeimage.img_1,
                img_2=storeimage.img_2,
                img_3=storeimage.img_3,
            )
            for storeimage in storeimages.itertuples()
        ]
        models.StoreImage.objects.bulk_create(storeimages_bulk)
        print("[+] Done")


    def handle(self, *args, **kwargs):
        self._initialize()