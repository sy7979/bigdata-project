from django.contrib import admin
from .models import Store, User, Menu, Review, Prescription

# Register your models here.
admin.site.register(Store)
admin.site.register(User)
admin.site.register(Menu)
admin.site.register(Review)
admin.site.register(Prescription)