# README

## 주의사항

> DateTimeField의 auto_now_add를 뺀 상태로 먼저 migration 해줘야한다.

```bash
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py initialize
# models.py - Review의 reg_time column에 auto_now_add=True 추가
$ python manage.py makemigrations ### 만약 선택지가 나온다면 2번 선택
$ python manage.py migrate
```



## Installation

```bash
$ pip install django-rest-auth
$ pip install django-cors-headers
$ pip install -U drf-yasg
$ pip install django-allauth
$ pip install djangorestframework-jwt
$ pip install beautifulsoup4
```



## Bug Fix

### ValueError: : "Review.user" must be a "User" instance.

```python
print("[*] Initializing review...")
models.Review.objects.all().delete()
reviews = dataframes["reviews"]
reviews_bulk = [
    models.Review(
        store=models.Store.objects.get(id=review.store),
        user=models.User.objects.get(username=review.user), ##
        score=review.score,
        content=review.content,
        reg_time=review.reg_time,
    )
    for review in reviews.itertuples()
]
models.Review.objects.bulk_create(reviews_bulk)
print("[+] Done")
```



### ValueError: cannot convert float NaN to integer

Not a Number 문제로 data 자체에 NaN이 들어있어 발생한 오류

=> IntegerField를 CharField로 바꿔서 해결



### django.db.utils.OperationalError: no such table: allauth_socialaccount

migration을 다시 해줘야함



### RuntimeWarning: DateTimeField received a naive datetime

datetime과 timezone이 달라서 발생하는 오류