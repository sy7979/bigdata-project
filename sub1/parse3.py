import json
import pandas as pd
import os
import shutil


DATA_DIR = "../data"
DATA_FILE = os.path.join(DATA_DIR, "data3.json")
DUMP_FILE = os.path.join(DATA_DIR, "dump3.pkl")

store_image_columns = (
    "id",  # 이미지 고유 번호
    "img_1",  # 이미지 url 1
    "img_2",  # 이미지 url 2
    "img_3",  # 이미지 url 3
)


def import_data(data_path=DATA_FILE):
    try:
        with open(data_path, encoding="utf-8") as f:
            data = json.loads(f.read())
    except FileNotFoundError as e:
        print(f"`{data_path}` 가 존재하지 않습니다.")
        exit(1)

    store_images = []  # 이미지

    for d in data:
        store_images.append(
            [
                d["id"],
                d["img_1"],
                d["img_2"],
                d["img_3"],
            ]
        )

    store_images_frame = pd.DataFrame(
        data=store_images, columns=store_image_columns)
    return {"store_images": store_images_frame}


def dump_dataframes(dataframes):
    pd.to_pickle(dataframes, DUMP_FILE)


def load_dataframes():
    return pd.read_pickle(DUMP_FILE)


def main():

    print("[*] Parsing data...")
    data = import_data()
    print("[+] Done")

    print("[*] Dumping data...")
    dump_dataframes(data)
    print("[+] Done\n")

    data = load_dataframes()
    
    term_w = shutil.get_terminal_size()[0] - 1
    separater = "-" * term_w

    print("[이미지]")
    print(f"{separater}\n")
    print(data["store_images"].head())
    print(f"\n{separater}\n\n")


if __name__ == "__main__":
    main()