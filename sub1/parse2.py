import json
import pandas as pd
import os
import shutil


DATA_DIR = "../data"
DATA_FILE = os.path.join(DATA_DIR, "data2.json")
DUMP_FILE = os.path.join(DATA_DIR, "dump2.pkl")

prescription_columns = (
    "name",  # 병명
    "symptom",  # 병증내용
    "ingredient",  # 재료처방
    "care",  # 처방내용
)


def import_data(data_path=DATA_FILE):
    try:
        with open(data_path, encoding="utf-8") as f:
            data = json.loads(f.read())
    except FileNotFoundError as e:
        print(f"`{data_path}` 가 존재하지 않습니다.")
        exit(1)

    prescriptions = []  # 처방

    for d in data:
        prescriptions.append(
            [
                d["병명"],
                d["병증내용"],
                d["처방"],
                d["주치내용"],
            ]
        )

    prescriptions_frame = pd.DataFrame(
        data=prescriptions, columns=prescription_columns)
    return {"prescriptions": prescriptions_frame}


def dump_dataframes(dataframes):
    pd.to_pickle(dataframes, DUMP_FILE)


def load_dataframes():
    return pd.read_pickle(DUMP_FILE)


def main():

    print("[*] Parsing data...")
    data = import_data()
    print("[+] Done")

    print("[*] Dumping data...")
    dump_dataframes(data)
    print("[+] Done\n")

    data = load_dataframes()
    
    term_w = shutil.get_terminal_size()[0] - 1
    separater = "-" * term_w

    print("[처방]")
    print(f"{separater}\n")
    print(data["prescriptions"].head())
    print(f"\n{separater}\n\n")


if __name__ == "__main__":
    main()