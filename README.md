# SSAFY Bigdata project

## How to Run

```
1. sub1 폴더에서 python parse.py 실행
   sub1 폴더에서 python parse2.py 실행
2. sub2/backend/api/models.py중 Review 모델을
    reg_time = models.DateTimeField(auto_now_add=True) 에서
    reg_time = models.DateTimeField()으로 수정
3. sub2/backend에서 아래에 나온 순서대로 실행
	$ pip install -r requirements.txt
	$ python manage.py makemigrations
	$ python manage.py migrate
	$ python manage.py initialize
### 참고사항 initialize는 엄청 오래 걸립니다. ###
### Warning은 timezone과 관련한 것입니다. 그대로 진행해주세요. ###
4. 전에 수정했던 reg_time을 원상복귀 시킵니다.
	reg_time = models.DateTimeField(auto_now_add=True)
5. 다시 아래에 나온 순서대로 진행
	$ python manage.py makemigrations
	$ python manage.py migrate
6. 서버 실행
	$ python manage.py runserver
```



### Sub1

```sh
cd sub1
pip install -r requirements.txt
python parse.py
python analyse.py
python visualize.py
```

### Sub 2

**Backend**

```sh
cd sub2/backend
pip install -r requirements.txt
python manage.py makemigrations
python manage.py migrate
python manage.py initialize
python manage.py runserver
```

**Frontend**

```sh
cd sub2/frontend
npm install
npm run serve
```
