# 1. Docker

### Docker 란 무엇일까

- **"Docker"란 컨테이너를 활용한 오픈소스 가상화 플랫폼** 

- **컨테이너**는 응용프로그램의 구동환경을 별도로 격리한 공간

- Docker는 이러한 컨테이너들을 구동시켜주는 역할을 함. 

  컨테이너 자체가 독립적인 구동환경을 갖고있기에 플랫폼에 종속적이지 않으며, 프로그램을 구동해야하는 환경을 서버마다 설치할 필요없이 도커로만 구동할 수 있다는 것이 장점.

- example)

  Window에서 제작한 Springboot 백엔드와 React 프론트엔드 환경을 도커 이미지로 제작하고, AWS 서버상에 해당 도커 이미지를 실행시키면, 서버는 스프링 혹은 리액트 환경이 없고 Linux 환경임에도 불구하고 제작했던 프로젝트들이 정상적으로 구동되는 시스템. 

  **"이미지 파일만 잘 만들어 놓으면 로컬개발환경과 동일하게 실제 배포환경에서도 프로젝트를 구동할 수 있다."**



### 시스템 인프라&시스템의 이용형태

- 시스템 인프라는 일반적으로 응용프로그램을 가동시키기 위한 **하드웨어, OS, 네트워크, 미들웨어** 등을 의미하며, 일반적으로 '기능요구사항'과 '비기능 요구사항'으로 나뉨

- 이러한 시스템을 우리는 '온프레미스'와 '클라우드' 라는 것으로 분류하여 사용하고 있음,

  **"온프레미스"** 는 일반적으로 기업과 같은 곳에서 자사 내에 거대한 서버를 두고시스템 구축부터 운용까지 모든것을 자체적으로 사용하는 시스템 이용체계를 의미함.

  \-  이러한 체계는 초기 시스템의 구축비용이 크고, 운용에 드는 유지보수 비용도 부담해야함.

  \-  적합한 경우 : 높은 가용성(절대 끊어지면 안되는 경우), 혹은 높은 기밀성을 요구하는 경우

  "**클라우드**" 시스템은 아마존 웹서비스(AWS), Azure 와 같이 사용자가 기반체계를 직접 구축하여 보유하지 않고, 서버와 네트워크등의 기반체계를 제공하는 플랫폼에 일정 금액을 지불하고 사용하는 체계

  \- 이는 특정사용자에게만 제공되는 "프라이빗"클라우드와 불특정 사용자에게 제공되는 "퍼블릭"클라우드로 분류되며단기간에 인프라를 구축할 수 있고, 서비스 이용량 만큼만 요금이 부과되기때문에 비용이 절감.

  -적합한 경우 : 트래픽 변동이 많은 경우(고객용 시스템), 재해 등의 초자연적인 현상으로 해외에 백업 구축이 필요한 경우. 서비스를 빠르게 제공하고 싶은 경우



### 쿠버네티스란?

- 도커가 컨테이너를 활용한 기술이라면 쿠버네티스는 이러한 도커들을 관리해주는 솔루션.

  노드들을 생성해서 노드마다 **컨테이너들을 관리하고 예약해주는 시스템**을 구축.



# 2. AWS 서버접속 & 배치파일 만들기

 **아마존 웹 서비스(Amazon Web Services : AWS)** 는 아마존에서 런칭한 클라우드 서비스로 최근 ICT인프라 시스템의 운영방식이 온프레미스방식 뿐만아니라 클라우드 시스템 사용이 대두됨으로 요새 잘쓰임.

 일반적으로 AWS 클라우드로 서버를 구축한다면 **Amazon Elastic Compute Cloud(AWS EC2)**를 사용할 겁니다.

**배치파일을 만들고 pem 파일을 이용해서 aws에 바로 접속할 수 있도록**



### PEM 파일 만들기

1. aws_test 폴더 만들기

2. AWS로 부터 받은 인증키를 pem 인증방식으로 저장(저장은 꼭 모든파일로 바꾸고 확장자를 pem으로 바꿔서 저장하기)

   ![image-20200402161649150](assets/image-20200402161649150.png)

### 배치(Batch file)파일 만들기

1. 같은 폴더 안에 아래와 같은 방식으로 배치파일 만들기

   ![image-20200402161840272](assets/image-20200402161840272.png)

   > echo on 은 실제 cmd 창을 키고 입력하는 현재 위치를 노출시킬것인지 아닌지 표시
   >
   > 아랫줄은 cmd 에서 ssh통신으로 입력하는 커맨드를 미리 입력해 놓은것
   >
   > ex) ssh -i ./[test.pem](https://zunoxi.tistory.com/test.pem) ubuntu@00.00.00.00(aws ip)

   ![image-20200402162051043](assets/image-20200402162051043.png)

   > 위와 같은 폴더구성이 되어야함

   - 배치파일을 사용함으로서 기존에 CMD 창을 열어 입력할 것을 미리 작성하여 바로 접속하는방식이며 배치파일 실행 시

     ***C:\Zunoxi\Desktop\aws_test>ssh -i ./[test.pem](https://zunoxi.tistory.com/test.pem) ubuntu@00.000.00.000***

     를 CMD에 입력한 것과 같은 결과가 나오게 됨.

### AWS 접속하기

생성된 배치 파일을 실행해보면

![image-20200402162226496](assets/image-20200402162226496.png)

![image-20200402162240241](assets/image-20200402162240241.png)



# 3. 윈도우에 Docker 설치하기

1. 가상화 기능 지원 확인

   본인의 윈도우가 가상환경을 지원하는지 확인해야함

   윈도우 10은 작업관리자의 가상화 지원여부에서 확인 가능

   ![image-20200403103417449](assets/image-20200403103417449.png)

2. Docker 다운로드

   윈도우 7과 윈도우 10 홈버전 이하면

   > https://docs.docker.com/toolbox/toolbox_install_windows/

   그 이상이라면

   > https://docs.docker.com/docker-for-windows/install/#download-docker-for-windows

3. Docker 설치

   docker 설치 전, 프로그램 및 기능에서 **Hyper-V**를 설치

   ![image-20200403103610059](assets/image-20200403103610059.png)

   도커 설치중![image-20200403103639416](assets/image-20200403103639416.png)

   설치후 재부팅 > 로그인 > 작업표시줄에 고래모양 아이콘 확인

   ![image-20200403103741253](assets/image-20200403103741253.png)

   cmd 창에  **docker version** 을 입력해서 확인

   ![image-20200403103828163](assets/image-20200403103828163.png)



# 4. 도커파일/이미지 생성

### Docker 환경 체크 명령어

* 도커 버전 확인

```
docker version
```

\* 도커 실행 환경 확인

```
docker system  info
```

\* 도커 디스크 이용 현황 확인

```
docker system df
```

\* 실행중인 도커 이미지 파일 확인

```
docker ps 
```

\* 실행중인 도커 이미지 파일 중지 

```
docker stop [도커 id]
```



### Dockerfile 만들기

**dockerfile** 이란 도커 이미지를 만드는 과정에서 ''이거를 기반으로 이미지를 만들어라'' 라고 방법 지정해주는것

**example**

```
From node:10

WORKDIR /src/app

COPY package*.json ./

RUN yarn install

COPY . .

CMD ["yarn","run","start"]
```

> \- **FROM** : 이 부분은 docker 이미지를 생성할 때 Docker Hub에 있는 해당 이미지를 기반으로 이미지 생성하겠다! 라고 선언 해주는 부분.  리액트 프로젝트여서 도커이미지로 만들기 위해 node:10이라고 작성.  
>
> \- **WORKDIR** : 이는 run, cmd 명령어가 실행될 디렉토리를 설정.
>
> \- **COPY** : 파일을 이미지에 추가합니다.  package.json을 추가함으로 파일에 명시되어있는
>
> 라이브러리가 다 추가될 수 있도록 설정한 것임.
>
> \- **RUN** : 우리가 구상할 이미지에 다른패키지를 설치.
>
> node:10 환경에 yarn install을 함으로써 package.json의 라이브러리를 install 하게 됨.  
>
> \- **CMD** : docker run을 실행시킬 시 도커에 사용할 default 명령어.



### 도커 이미지 파일 생성

1. Dockerfile이 있는 폴더에서 터미널 창을 연다.

2. ```
   docker build -f Dockerfile.dev -t 이미지태그 .
   ```

   docker 이미지 파일을 만드는데에 있어 -f는 참조할 docker 파일명, -t는 이미지파일에 붙일 태그를 생성.(뒤에 꼭 "." !!!)

   ![image-20200403104632792](assets/image-20200403104632792.png)

3. 터미널 창에서 docker image 확인

   ```bash
   docker images
   ```

   ![image-20200403104722021](assets/image-20200403104722021.png)

   이렇게 이미지태그 라는 레포지토리 생성된것을 확인할수 있음

   

# Docker Hub push/pull/run

만든 도커 이미지를 Docker Hub 에 push pull 해서 **로컬지역에서 개발한 환경**을 도커 이미지를 로딩함으로써 **서버에서도 구동**이 가능하게 해야함.

1. docker push하기

   \*  **docker hub**는 git hub이 소스공유, 관리 시스템인것 처럼 **docker이미지를 공유 및 버전 관리**를 하게 해주는 사이트.

   현재 만들어진 docker 이미지 파일들 확인. 

   ```
   > docker images
   ```

   ![image-20200403110941537](assets/image-20200403110941537.png)

   여기서 원하는 파일 push 하면 됨.

   > docker push 하려면 docker hub 에 계정이 있어야하고,  **Repository 가 있어야 push 할 수 있음**
   >
   > 예시에서는 javer repository 만들고 이곳에 로컬에서 만든 image 파일을 push 할예정
   >
   > ![image-20200403111153768](assets/image-20200403111153768.png)

   먼저 docker hub에 업로드 할 수 있는 권한을 얻기 위해 터미널 창에  명령어 입력.

   ```
   > docker login
   ```

   이후 userID, password를 입력, Login succeed 확인

   \* **Docker image 파일 태그 걸기**

   ```
   > docker tag zunoxi cross9308/javer:zunoxi
   ```

   docker tag [이미지 명] [docker hub id]/[repository 명]:[태그명] 이렇게 입력해 주시면 되겠습니다.  

   *** Docker image 파일 푸시 하기**

   ```
   > docker push cross9308/javer:zunoxi
   ```

   docker push [docker hub id]/[repository 명]:[태그명] 

   > denied 가 뜬다면
   >
   > https://stackoverflow.com/questions/41984399/denied-requested-access-to-the-resource-is-denied-docker

   docker hub 에 업로드 되었는지 확인

   ![image-20200403111500469](assets/image-20200403111500469.png)



2. pull 당기기

   AWS 서버에 접속하여 커맨드 창에 다음과 같은 명령어를 입력하여 pull .

   **(Docker login 마찬가지로 사전에 해줘야 함!)**

   ```bash
   docker pull [docker hub id]/[repository 명]:[태그명] 
   ```

   ![image-20200403111551175](assets/image-20200403111551175.png)

   ![image-20200403111609680](assets/image-20200403111609680.png)

3. docker image run

   다음과 같은 명령어로 간단하게 실행 가능.

   ```
   $ docker run -p 3000:3000 944f15368886
   docker run -p [내부포트]:[외부포트] [docker imageID] 
   ```







**Basic Concept**



![img](https://t1.daumcdn.net/cfile/tistory/99A2523D5C910CD90B)



평점 행렬을 사용자와 아이템 Latent Factor 행렬로 분해



![img](https://t1.daumcdn.net/cfile/tistory/996A2D4F5C9110B52B)



R은 사용자가 아이템에 남긴 평점을 행렬로 나타낸 것입니다.

R의 크기는 사용자 수(Nu) x 아이템의 수(Ni)가 됩니다.



![img](https://t1.daumcdn.net/cfile/tistory/998489455C910F8E22)



Nf는 Matrix Factorization 학습 시에 정하는 임의의 차원 수입니다.

개발자가 조정 가능하며 보통 50에서 200 사이로 설정합니다.

X와 Y는 사용자와 아이템의 Latent Factor 행렬을 나타내며, 우리가 학습시키고자 하는 대상입니다.

이 행렬들의 값은 아주 작은 랜덤한 값들로 초기화됩니다. (R에 있는 값을 쪼개어 생성하는 것이 아닙니다!)



![img](https://t1.daumcdn.net/cfile/tistory/995A74505C910FFC20)



xu 와 yi는 각각 특정 사용자와 특정 아이템의 특징 벡터를 나타냅니다.

여기서 벡터는 1차원 어레이를 뜻하며, xu와 yi는 각각 Nf x 1 크기의 열 벡터 (column vector)로 볼 수 있습니다.

앞으로 xu를 특정 사용자의 Latent Vector, yi를 특정 아이템의 Latent Vector라고 하겠습니다.



![img](https://t1.daumcdn.net/cfile/tistory/990FBA3A5C910D0A1A)

분해된 행렬을 다시 곱하여 예측 평점 행렬을 계산

![img](https://t1.daumcdn.net/cfile/tistory/99E6F4495C9110F116)



X 행렬의 전치 행렬 (Transpose)를 구하면 Nu x Nf 크기의 행렬이 됩니다.

이를 Y 행렬과 곱해주어 Nu x Ni 크기의 평점 예측 행렬을 구합니다.

예측된 하나의 평점 행렬의 각각의 값들은 다음과 같은 곱셈을 통해서 계산됩니다.



![img](https://t1.daumcdn.net/cfile/tistory/99B56E4D5C9111B72A)

예측 평점 수식



해석해보면 특정 사용자의 Latent Vector와 특정 아이템의 Latent Vector를 곱하여 평점을 계산함을 알 수 있습니다.

Latent Factor Matrix가 적절하게 학습이 되었다면 예측된 R'은 R과 유사한 결과를 낼 것이라 예상할 수 있습니다.





**Loss Function**



이제 우리는 X와 Y를 학습시키기 위한 Loss Fucntion을 구할 수 있습니다.

바로 X와 Y를 곱하여 얻은 예측 평점 행렬의 오차가 최대한 작아지도록 수식을 구성하면 됩니다.





![img](https://t1.daumcdn.net/cfile/tistory/99F069465C91136E2C)



Loss Function



수식의 앞 부분은 예측된 평점과 실제 평점 간의 차이를 나타냅니다.

그리고 뒷 부분에 람다 표기가 붙은 부분은 모델이 오버 피팅 하지 않도록 정규화 시켜주는 역할을 수행합니다.

즉, 전체 모델에서 파라미터가 미치는 영향력을 감소시켜 줌으로써 모델의 일반화를 돕습니다.

람다의 크기는 데이터 셋마다 달라질 수 있으므로, 여러 실험을 통해 조절해 나가는 것이 바람직합니다.





**Optimization**





Loss Function도 구했으니 이제 이를 최소화하도록 X와 Y를 학습시킬 일만 남았습니다.

그러한 최적화를 수행하는 알고리즘으로 **Gradient Descent**와 **Alternating Least Squares** 알고리즘이 있습니다.



Gradient Descent를 적용하는 방식은 간단합니다. (아마 딥 러닝을 공부해보신 분들은 익숙하실 것 같습니다.)

loss를 미분하고, 여기에 learning rate를 곱한 다음 X와 Y 행렬의 값들을 업데이트 해주는 방식입니다.



개념적으로는 금방 와닿지만 막상 계산은 상당히 복잡하여 여기서는 다루지 않도록 하겠습니다. 

궁금하신 분들은 Andrew ng 교수님의 다음 강의를 참고하시면 좋을 듯 합니다. [1]



![img](https://t1.daumcdn.net/cfile/tistory/990E9F435C911F3631)

Gradient Update 수식



하지만 여기서 우리가 학습시켜야 하는 것은 X와 Y 행렬 두 개이고, 이 둘은 곱셉으로 묶여있습니다. (xuT x yi)

이 둘을 동시에 최적화 시키는 문제는 Non-convex problem으로 NP에 속합니다. 

(NP, Non-convex는 별도로 자세히 다룰 예정입니다.)

Gradient Descent로 이를 최적화 시키는 것은 너무 느리고 많은 반복이 필요하다는 단점이 있습니다. [2]



**Alternatin Least Squares** 는 이러한 문제를 멋지게 해결합니다.

X와 Y 둘 중 하나를 고정시키고 다른 하나를 최적화 시킵니다. 

그리고 이 과정을 번갈아가며 반복하여 짧은 시간 내에 최적의 X와 Y를 찾아냅니다.





추천 모델은 CF(Collaborative Filtering) 모델을 사용할 예정이다.

![img](https://t1.daumcdn.net/cfile/tistory/99342B355C73F6B013)

CF 모델을 사용하기 위해서는 고객들간의 관계가 필요한데, 이를 위해 2개 이상의 구매이력을 가진 고객 데이터만 뽑아놓은 데이터가 "cos_2more.csv"데이터이다. (1개만 구매한 고객들로는 이후 사용할 KNN알고리즘에 적용하기 어렵다.)



총 2,887건의 고객데이터를 보유하고 있다.

변수는 16개가 나오는데, 이 중 CF모델에 필요한 데이터인 유저이름, 제품명, 평점 3가지 변수들만 뽑아오도록 한다.

![img](https://t1.daumcdn.net/cfile/tistory/992827365C73F78917)

nickname, product_name 변수들은 object형, rating 변수는 int형 변수이다.

그리고 보유한 데이터가 테이블 형태로 되어있는데, 추천시스템에 사용하기 위해서는 데이터를 딕셔너리 형태로 바꾸어 주어야 한다. 그래야 머신러닝 모델을 통해 나온 결과를 인덱싱해서 추천 화장품 리스트로 뽑아낼 수 있다.

![img](https://t1.daumcdn.net/cfile/tistory/99A2344A5C7402A912)

![img](https://t1.daumcdn.net/cfile/tistory/991D87415C73F82246)

![img](https://t1.daumcdn.net/cfile/tistory/992CAC345C73F83A0D)

   ![img](https://t1.daumcdn.net/cfile/tistory/9948463D5C73F8A20A)

이제 딕셔너리 구성이 완료되었다. 이 딕셔너리를 바탕으로 사용자 닉네임을 입력하면 추천해주는 모델을 구현해보자.

![img](https://t1.daumcdn.net/cfile/tistory/999CF3465C73F9F125)



surprise 라이브러리에 rating_scale을 1~5점 척도로 지정해주었다.

학습에 사용된 머신러닝 알고리즘은 KNN이다. 나중에 K값을 넣어주면 그에 맞게 추천 알고리즘이 발현된다.

그리고 사용자간 유사도를 측정하는데에는 pearson similarity를 사용하였다. 

![img](https://t1.daumcdn.net/cfile/tistory/997E2B4B5C73FB6606)

최종적으로 fit으로 학습시키면 모델이 완성된다.

이제 이 모델을 바탕으로 "**닉네임a**"라는 고객의 추천리스트를 만들어보자.

![img](https://t1.daumcdn.net/cfile/tistory/9937563F5C74034B17)

K=5로 설정되어서 유사한 사용자 5명을 기반으로 화장품을 추천해주게 된다.

Input창에 유저 닉네임을 입력한다면 그에 맞는 인덱스 번호 및 유사 사용자 번호를 보여주고, 이를 바탕으로 추천하는 화장품 목록을 보여준다.



**<결론>**

해당 고객인 "닉네임a"에게는 MLE로션, 에센셜 모이스처라이저, 로열허니 착한 에멀전, 수분가득 콜라겐 에멀전, 더테라피 에센셜 포뮬러 에멀전, 슈퍼 아쿠아 울트라 워터-풀 컨트롤 에멀전이 추천되었다. 이 화장품들은 주로 수분공급용 제품들이다.

![img](https://t1.daumcdn.net/cfile/tistory/995346435C74013301)

실제로 닉네임a에 대한 피부타입을 살펴보면 건성이다. 건성에 맞는 수분공급용 제품들이 추천된 것을 확인할 수 있다.

닉네임a의 제품 선호도와 유사한 사용자 5명을 선정한 후 그 사용자들이 선택한 다른 제품들을 닉네임a에게 추천해주는 것이다.



**<추천시스템에 대하여>**

이것으로 화장품 추천 모델에 대한 것이 일단락되었다. 하지만 이 CF추천시스템은 단점이 있다. 과거 구매이력 데이터가 적은 고객에게는 추천이 힘들다는 것이다. 모델이 그들의 소비자 데이터를 충분히 학습하지 못해서 추천하는데 한계가 있다.

이를 보완할 수 있는 추천 시스템이 CBF(Content-based Filtering) 추천시스템이다. 제품 정보와 소비자 정보에 대한 매칭을 통해 제품을 추천하므로 이전 데이터가 충분하지 않아도 추천이 가능하다.

CF와 CBF가 하이브리드된 추천 모델을 만들면 훨씬 더 강력한 추천시스템이 만들어질 수 있을 것이라는 가설이 나온다.

![img](https://t1.daumcdn.net/cfile/tistory/995B33505C7401EA24)