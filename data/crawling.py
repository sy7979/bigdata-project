#-- coding:utf-8 --
# pip install selenium

from selenium import webdriver
from bs4 import BeautifulSoup
import time

url = 'https://www.diningcode.com/isearch.php?query=사랑방'
print("OK")
driver = webdriver.Chrome(executable_path="chromedriver.exe")
driver.get(url)
html_source = driver.page_source
print(html_source)
driver.close()
soup = BeautifulSoup(html_source, 'html.parser')
print(soup)
stores = soup.select("#div_rn > ul > li:nth-child(2) > a")
print(stores)