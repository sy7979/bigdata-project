import json
import pandas as pd
import xlrd
from collections import OrderedDict

data_list = []
data_dict = {}

excel_file_path_1 = "../data/대표식품별_건강기능_병증처방_정보_1_20190305.xls"
excel_file_path_2 = "../data/대표식품별_건강기능_병증처방_정보_2_20190305.xls"
excel_file_path_3 = "../data/대표식품별_건강기능_병증처방_정보_3_20190305.xls"
excel_file_path_4 = "../data/대표식품별_건강기능_병증처방_정보_4_20190305.xls"

wb = xlrd.open_workbook(excel_file_path_1)
for i in range(3):
    sh = wb.sheet_by_index(i)

    for rownum in range(3, sh.nrows):
        data = OrderedDict()
        row_values = sh.row_values(rownum)
        
        if row_values[8] != "":
            data['병명'] = row_values[8]
            data['병증내용'] = row_values[13]
            data['처방'] = row_values[16]
            data['주치내용'] = row_values[22]
            try:
                if data_dict[row_values[8] + row_values[16]]:
                    pass
            except:
                data_dict[row_values[8] + row_values[16]] = True
                data_list.append(data)
        
wb = xlrd.open_workbook(excel_file_path_2)
for i in range(3):
    sh = wb.sheet_by_index(i)

    for rownum in range(3, sh.nrows):
        data = OrderedDict()
        row_values = sh.row_values(rownum)

        if row_values[8] != "":
            data['병명'] = row_values[8]
            data['병증내용'] = row_values[13]
            data['처방'] = row_values[16]
            data['주치내용'] = row_values[22]
            try:
                if data_dict[row_values[8] + row_values[16]]:
                    pass
            except:
                data_dict[row_values[8] + row_values[16]] = True
                data_list.append(data)

wb = xlrd.open_workbook(excel_file_path_3)
for i in range(3):
    sh = wb.sheet_by_index(i)

    for rownum in range(3, sh.nrows):
        data = OrderedDict()
        row_values = sh.row_values(rownum)

        if row_values[8] != "":
            data['병명'] = row_values[8]
            data['병증내용'] = row_values[13]
            data['처방'] = row_values[16]
            data['주치내용'] = row_values[22]
            try:
                if data_dict[row_values[8] + row_values[16]]:
                    pass
            except:
                data_dict[row_values[8] + row_values[16]] = True
                data_list.append(data)

wb = xlrd.open_workbook(excel_file_path_4)
for i in range(5):
    sh = wb.sheet_by_index(i)

    for rownum in range(3, sh.nrows):
        data = OrderedDict()
        row_values = sh.row_values(rownum)
        
        if row_values[8] != "":
            data['병명'] = row_values[8]
            data['병증내용'] = row_values[13]
            data['처방'] = row_values[16]
            data['주치내용'] = row_values[22]
            try:
                if data_dict[row_values[8] + row_values[16]]:
                    pass
            except:
                data_dict[row_values[8] + row_values[16]] = True
                data_list.append(data)


j = json.dumps(data_list, ensure_ascii=False)

with open('data2.json', 'w+', -1, "utf-8") as f:
    f.write(j)